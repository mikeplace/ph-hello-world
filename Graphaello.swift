// swiftlint:disable all
// This file was automatically generated and should not be edited.

import Apollo
import Combine
import Foundation
import SwiftUI

// MARK: Basic API

protocol Target {}

protocol API: Target {
    var client: ApolloClient { get }
}

extension API {
    func fetch<Query: GraphQLQuery>(query: Query, completion: @escaping (Result<Query.Data, GraphQLLoadingError<Self>>) -> Void) {
        client.fetch(query: query) { result in
            switch result {
            case let .success(result):
                guard let data = result.data else {
                    if let errors = result.errors, errors.count > 0 {
                        return completion(.failure(.graphQLErrors(errors)))
                    }
                    return completion(.failure(.emptyData(api: self)))
                }
                completion(.success(data))
            case let .failure(error):
                completion(.failure(.networkError(error)))
            }
        }
    }
}

protocol MutationTarget: Target {}

protocol Connection: Target {
    associatedtype Node
}

protocol Fragment {
    associatedtype UnderlyingType
    static var placeholder: Self { get }
}

extension Array: Fragment where Element: Fragment {
    typealias UnderlyingType = [Element.UnderlyingType]

    static var placeholder: [Element] {
        return Array(repeating: Element.placeholder, count: 5)
    }
}

extension Optional: Fragment where Wrapped: Fragment {
    typealias UnderlyingType = Wrapped.UnderlyingType?

    static var placeholder: Wrapped? {
        return Wrapped.placeholder
    }
}

protocol Mutation: ObservableObject {
    associatedtype Value

    var isLoading: Bool { get }
}

protocol CurrentValueMutation: ObservableObject {
    associatedtype Value

    var isLoading: Bool { get }
    var value: Value { get }
    var error: Error? { get }
}

// MARK: - Basic API: Paths

struct GraphQLPath<TargetType: Target, Value> {
    fileprivate init() {}
}

struct GraphQLFragmentPath<TargetType: Target, UnderlyingType> {
    fileprivate init() {}
}

extension GraphQLFragmentPath {
    typealias Path<V> = GraphQLPath<TargetType, V>
    typealias FragmentPath<V> = GraphQLFragmentPath<TargetType, V>
}

extension GraphQLFragmentPath {
    var _fragment: FragmentPath<UnderlyingType> {
        return self
    }
}

extension GraphQLFragmentPath {
    func _forEach<Value, Output>(_: KeyPath<GraphQLFragmentPath<TargetType, Value>, GraphQLPath<TargetType, Output>>) -> GraphQLPath<TargetType, [Output]> where UnderlyingType == [Value] {
        return .init()
    }

    func _forEach<Value, Output>(_: KeyPath<GraphQLFragmentPath<TargetType, Value>, GraphQLPath<TargetType, Output>>) -> GraphQLPath<TargetType, [Output]?> where UnderlyingType == [Value]? {
        return .init()
    }
}

extension GraphQLFragmentPath {
    func _forEach<Value, Output>(_: KeyPath<GraphQLFragmentPath<TargetType, Value>, GraphQLFragmentPath<TargetType, Output>>) -> GraphQLFragmentPath<TargetType, [Output]> where UnderlyingType == [Value] {
        return .init()
    }

    func _forEach<Value, Output>(_: KeyPath<GraphQLFragmentPath<TargetType, Value>, GraphQLFragmentPath<TargetType, Output>>) -> GraphQLFragmentPath<TargetType, [Output]?> where UnderlyingType == [Value]? {
        return .init()
    }
}

extension GraphQLFragmentPath {
    func _flatten<T>() -> GraphQLFragmentPath<TargetType, [T]> where UnderlyingType == [[T]] {
        return .init()
    }

    func _flatten<T>() -> GraphQLFragmentPath<TargetType, [T]?> where UnderlyingType == [[T]]? {
        return .init()
    }
}

extension GraphQLPath {
    func _flatten<T>() -> GraphQLPath<TargetType, [T]> where Value == [[T]] {
        return .init()
    }

    func _flatten<T>() -> GraphQLPath<TargetType, [T]?> where Value == [[T]]? {
        return .init()
    }
}

extension GraphQLFragmentPath {
    func _compactMap<T>() -> GraphQLFragmentPath<TargetType, [T]> where UnderlyingType == [T?] {
        return .init()
    }

    func _compactMap<T>() -> GraphQLFragmentPath<TargetType, [T]?> where UnderlyingType == [T?]? {
        return .init()
    }
}

extension GraphQLPath {
    func _compactMap<T>() -> GraphQLPath<TargetType, [T]> where Value == [T?] {
        return .init()
    }

    func _compactMap<T>() -> GraphQLPath<TargetType, [T]?> where Value == [T?]? {
        return .init()
    }
}

extension GraphQLFragmentPath {
    func _nonNull<T>() -> GraphQLFragmentPath<TargetType, T> where UnderlyingType == T? {
        return .init()
    }
}

extension GraphQLPath {
    func _nonNull<T>() -> GraphQLPath<TargetType, T> where Value == T? {
        return .init()
    }
}

extension GraphQLFragmentPath {
    func _withDefault<T>(_: @autoclosure () -> T) -> GraphQLFragmentPath<TargetType, T> where UnderlyingType == T? {
        return .init()
    }
}

extension GraphQLPath {
    func _withDefault<T>(_: @autoclosure () -> T) -> GraphQLPath<TargetType, T> where Value == T? {
        return .init()
    }
}

// MARK: - Basic API: Arguments

enum GraphQLArgument<Value> {
    enum QueryArgument {
        case withDefault(Value)
        case forced
    }

    case value(Value)
    case argument(QueryArgument)
}

extension GraphQLArgument {
    static var argument: GraphQLArgument<Value> {
        return .argument(.forced)
    }

    static func argument(default value: Value) -> GraphQLArgument<Value> {
        return .argument(.withDefault(value))
    }
}

// MARK: - Basic API: Paging

class Paging<Value: Fragment>: DynamicProperty, ObservableObject {
    fileprivate struct Response {
        let values: [Value]
        let cursor: String?
        let hasMore: Bool

        static var empty: Response {
            Response(values: [], cursor: nil, hasMore: false)
        }
    }

    fileprivate typealias Completion = (Result<Response, Error>) -> Void
    fileprivate typealias Loader = (String, Int?, @escaping Completion) -> Void

    private let loader: Loader

    @Published
    private(set) var isLoading: Bool = false

    @Published
    private(set) var values: [Value] = []

    private var cursor: String?

    @Published
    private(set) var hasMore: Bool = false

    @Published
    private(set) var error: Error? = nil

    fileprivate init(_ response: Response, loader: @escaping Loader) {
        self.loader = loader
        use(response)
    }

    func loadMore(pageSize: Int? = nil) {
        guard let cursor = cursor, !isLoading else { return }
        isLoading = true
        loader(cursor, pageSize) { [weak self] result in
            switch result {
            case let .success(response):
                self?.use(response)
            case let .failure(error):
                self?.handle(error)
            }
        }
    }

    private func use(_ response: Response) {
        isLoading = false
        values += response.values
        cursor = response.cursor
        hasMore = response.hasMore
    }

    private func handle(_ error: Error) {
        isLoading = false
        hasMore = false
        self.error = error
    }
}

// MARK: - Basic API: Error Types

enum GraphQLLoadingError<T: API>: Error {
    case emptyData(api: T)
    case graphQLErrors([GraphQLError])
    case networkError(Error)
}

// MARK: - Basic API: Refresh

protocol QueryRefreshController {
    func refresh()
    func refresh(completion: @escaping (Error?) -> Void)
}

private struct QueryRefreshControllerEnvironmentKey: EnvironmentKey {
    static let defaultValue: QueryRefreshController? = nil
}

extension EnvironmentValues {
    var queryRefreshController: QueryRefreshController? {
        get {
            self[QueryRefreshControllerEnvironmentKey.self]
        } set {
            self[QueryRefreshControllerEnvironmentKey.self] = newValue
        }
    }
}

// MARK: - Error Handling

enum QueryError {
    case network(Error)
    case graphql([GraphQLError])
}

extension QueryError: CustomStringConvertible {
    var description: String {
        switch self {
        case let .network(error):
            return error.localizedDescription
        case let .graphql(errors):
            return errors.map { $0.description }.joined(separator: ", ")
        }
    }
}

extension QueryError {
    var networkError: Error? {
        guard case let .network(error) = self else { return nil }
        return error
    }

    var graphQLErrors: [GraphQLError]? {
        guard case let .graphql(errors) = self else { return nil }
        return errors
    }
}

protocol QueryErrorController {
    var error: QueryError? { get }
    func clear()
}

private struct QueryErrorControllerEnvironmentKey: EnvironmentKey {
    static let defaultValue: QueryErrorController? = nil
}

extension EnvironmentValues {
    var queryErrorController: QueryErrorController? {
        get {
            self[QueryErrorControllerEnvironmentKey.self]
        } set {
            self[QueryErrorControllerEnvironmentKey.self] = newValue
        }
    }
}

// MARK: - Basic API: Views

private struct QueryRenderer<Query: GraphQLQuery, Loading: View, Error: View, Content: View>: View {
    typealias ContentFactory = (Query.Data) -> Content
    typealias ErrorFactory = (QueryError) -> Error

    private final class ViewModel: ObservableObject {
        @Published var isLoading: Bool = false
        @Published var value: Query.Data? = nil
        @Published var error: QueryError? = nil

        private var previous: Query?
        private var cancellable: Apollo.Cancellable?

        deinit {
            cancel()
        }

        func load(client: ApolloClient, query: Query) {
            guard previous !== query || (value == nil && !isLoading) else { return }
            perform(client: client, query: query)
        }

        func refresh(client: ApolloClient, query: Query, completion: ((Swift.Error?) -> Void)? = nil) {
            perform(client: client, query: query, cachePolicy: .fetchIgnoringCacheData, completion: completion)
        }

        private func perform(client: ApolloClient, query: Query, cachePolicy: CachePolicy = .returnCacheDataElseFetch, completion: ((Swift.Error?) -> Void)? = nil) {
            previous = query
            cancellable = client.fetch(query: query, cachePolicy: cachePolicy) { [weak self] result in
                defer {
                    self?.cancellable = nil
                    self?.isLoading = false
                }
                switch result {
                case let .success(result):
                    self?.value = result.data
                    self?.error = result.errors.map { .graphql($0) }
                    completion?(nil)
                case let .failure(error):
                    self?.error = .network(error)
                    completion?(error)
                }
            }
            isLoading = true
        }

        func cancel() {
            cancellable?.cancel()
        }
    }

    private struct RefreshController: QueryRefreshController {
        let client: ApolloClient
        let query: Query
        let viewModel: ViewModel

        func refresh() {
            viewModel.refresh(client: client, query: query)
        }

        func refresh(completion: @escaping (Swift.Error?) -> Void) {
            viewModel.refresh(client: client, query: query, completion: completion)
        }
    }

    private struct ErrorController: QueryErrorController {
        let viewModel: ViewModel

        var error: QueryError? {
            return viewModel.error
        }

        func clear() {
            viewModel.error = nil
        }
    }

    let client: ApolloClient
    let query: Query
    let loading: Loading
    let error: ErrorFactory
    let factory: ContentFactory

    @ObservedObject private var viewModel = ViewModel()
    @State private var hasAppeared = false

    var body: some View {
        if hasAppeared {
            self.viewModel.load(client: self.client, query: self.query)
        }
        return VStack {
            viewModel.isLoading && viewModel.value == nil && viewModel.error == nil ? loading : nil
            viewModel.value == nil ? viewModel.error.map(error) : nil
            viewModel
                .value
                .map(factory)
                .environment(\.queryRefreshController, RefreshController(client: client, query: query, viewModel: viewModel))
                .environment(\.queryErrorController, ErrorController(viewModel: viewModel))
        }
        .onAppear {
            DispatchQueue.main.async {
                self.hasAppeared = true
            }
            self.viewModel.load(client: self.client, query: self.query)
        }
        .onDisappear {
            DispatchQueue.main.async {
                self.hasAppeared = false
            }
            self.viewModel.cancel()
        }
    }
}

private struct BasicErrorView: View {
    let error: QueryError

    var body: some View {
        Text("Error: \(error.description)")
    }
}

private struct BasicLoadingView: View {
    var body: some View {
        Text("Loading")
    }
}

struct PagingView<Value: Fragment>: View {
    enum Mode {
        case list
        case vertical(alignment: HorizontalAlignment = .center, spacing: CGFloat? = nil, insets: EdgeInsets = EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
        case horizontal(alignment: VerticalAlignment = .center, spacing: CGFloat? = nil, insets: EdgeInsets = EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
    }

    enum Data {
        case item(Value, Int)
        case loading
        case error(Error)

        fileprivate var id: String {
            switch self {
            case let .item(_, int):
                return int.description
            case .error:
                return "error"
            case .loading:
                return "loading"
            }
        }
    }

    @ObservedObject private var paging: Paging<Value>
    private let mode: Mode
    private let pageSize: Int?
    private var loader: (Data) -> AnyView

    @State private var visibleRect: CGRect = .zero

    init(_ paging: Paging<Value>, mode: Mode = .list, pageSize: Int? = nil, loader: @escaping (Data) -> AnyView) {
        self.paging = paging
        self.mode = mode
        self.pageSize = pageSize
        self.loader = loader
    }

    var body: some View {
        let data = self.paging.values.enumerated().map { Data.item($0.element, $0.offset) } +
            [self.paging.isLoading ? Data.loading : nil, self.paging.error.map(Data.error)].compactMap { $0 }

        switch mode {
        case .list:
            return AnyView(
                List(data, id: \.id) { data in
                    self.loader(data).onAppear { self.onAppear(data: data) }
                }
            )
        case let .vertical(alignment, spacing, insets):
            return AnyView(
                ScrollView(.horizontal, showsIndicators: false) {
                    VStack(alignment: alignment, spacing: spacing) {
                        ForEach(data, id: \.id) { data in
                            self.loader(data).ifVisible(in: self.visibleRect, in: .named("InfiniteVerticalScroll")) { self.onAppear(data: data) }
                        }
                    }
                    .padding(insets)
                }
                .coordinateSpace(name: "InfiniteVerticalScroll")
                .rectReader($visibleRect, in: .named("InfiniteVerticalScroll"))
            )
        case let .horizontal(alignment, spacing, insets):
            return AnyView(
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(alignment: alignment, spacing: spacing) {
                        ForEach(data, id: \.id) { data in
                            self.loader(data).ifVisible(in: self.visibleRect, in: .named("InfiniteHorizontalScroll")) { self.onAppear(data: data) }
                        }
                    }
                    .padding(insets)
                }
                .coordinateSpace(name: "InfiniteHorizontalScroll")
                .rectReader($visibleRect, in: .named("InfiniteHorizontalScroll"))
            )
        }
    }

    private func onAppear(data: Data) {
        guard !paging.isLoading,
            paging.hasMore,
            case let .item(_, index) = data,
            index > paging.values.count - 2 else { return }

        DispatchQueue.main.async {
            paging.loadMore(pageSize: pageSize)
        }
    }
}

extension PagingView {
    init<Loading: View, Error: View, Data: View>(_ paging: Paging<Value>,
                                                 mode: Mode = .list,
                                                 pageSize: Int? = nil,
                                                 loading loadingView: @escaping () -> Loading,
                                                 error errorView: @escaping (Swift.Error) -> Error,
                                                 item itemView: @escaping (Value) -> Data) {
        self.init(paging, mode: mode, pageSize: pageSize) { data in
            switch data {
            case let .item(item, _):
                return AnyView(itemView(item))
            case let .error(error):
                return AnyView(errorView(error))
            case .loading:
                return AnyView(loadingView())
            }
        }
    }

    init<Error: View, Data: View>(_ paging: Paging<Value>,
                                  mode: Mode = .list,
                                  pageSize: Int? = nil,
                                  error errorView: @escaping (Swift.Error) -> Error,
                                  item itemView: @escaping (Value) -> Data) {
        self.init(paging,
                  mode: mode,
                  pageSize: pageSize,
                  loading: { PagingBasicLoadingView(content: itemView) },
                  error: errorView,
                  item: itemView)
    }

    init<Loading: View, Data: View>(_ paging: Paging<Value>,
                                    mode: Mode = .list,
                                    pageSize: Int? = nil,
                                    loading loadingView: @escaping () -> Loading,
                                    item itemView: @escaping (Value) -> Data) {
        self.init(paging,
                  mode: mode,
                  pageSize: pageSize,
                  loading: loadingView,
                  error: { Text("Error: \($0.localizedDescription)") },
                  item: itemView)
    }

    init<Data: View>(_ paging: Paging<Value>,
                     mode: Mode = .list,
                     pageSize: Int? = nil,
                     item itemView: @escaping (Value) -> Data) {
        self.init(paging,
                  mode: mode,
                  pageSize: pageSize,
                  loading: { PagingBasicLoadingView(content: itemView) },
                  error: { Text("Error: \($0.localizedDescription)") },
                  item: itemView)
    }
}

private struct PagingBasicLoadingView<Value: Fragment, Content: View>: View {
    let content: (Value) -> Content

    var body: some View {
        if #available(iOS 14.0, macOS 11.0, tvOS 14.0, watchOS 7.0, *) {
            content(.placeholder).disabled(true).redacted(reason: .placeholder)
        } else {
            BasicLoadingView()
        }
    }
}

extension PagingView.Mode {
    static func vertical(alignment: HorizontalAlignment = .center, spacing: CGFloat? = nil, padding edges: Edge.Set, by padding: CGFloat) -> PagingView.Mode {
        return .vertical(alignment: alignment,
                         spacing: spacing,
                         insets: EdgeInsets(top: edges.contains(.top) ? padding : 0,
                                            leading: edges.contains(.leading) ? padding : 0,
                                            bottom: edges.contains(.bottom) ? padding : 0,
                                            trailing: edges.contains(.trailing) ? padding : 0))
    }

    static func vertical(alignment: HorizontalAlignment = .center, spacing: CGFloat? = nil, padding: CGFloat) -> PagingView.Mode {
        return .vertical(alignment: alignment, spacing: spacing, padding: .all, by: padding)
    }

    static var vertical: PagingView.Mode { .vertical() }

    static func horizontal(alignment: VerticalAlignment = .center, spacing: CGFloat? = nil, padding edges: Edge.Set, by padding: CGFloat) -> PagingView.Mode {
        return .horizontal(alignment: alignment,
                           spacing: spacing,
                           insets: EdgeInsets(top: edges.contains(.top) ? padding : 0,
                                              leading: edges.contains(.leading) ? padding : 0,
                                              bottom: edges.contains(.bottom) ? padding : 0,
                                              trailing: edges.contains(.trailing) ? padding : 0))
    }

    static func horizontal(alignment: VerticalAlignment = .center, spacing: CGFloat? = nil, padding: CGFloat) -> PagingView.Mode {
        return .horizontal(alignment: alignment, spacing: spacing, padding: .all, by: padding)
    }

    static var horizontal: PagingView.Mode { .horizontal() }
}

extension View {
    fileprivate func rectReader(_ binding: Binding<CGRect>, in space: CoordinateSpace) -> some View {
        background(GeometryReader { (geometry) -> AnyView in
            let rect = geometry.frame(in: space)
            DispatchQueue.main.async {
                binding.wrappedValue = rect
            }
            return AnyView(Rectangle().fill(Color.clear))
        })
    }
}

extension View {
    fileprivate func ifVisible(in rect: CGRect, in space: CoordinateSpace, execute: @escaping () -> Void) -> some View {
        background(GeometryReader { (geometry) -> AnyView in
            let frame = geometry.frame(in: space)
            if frame.intersects(rect) {
                execute()
            }
            return AnyView(Rectangle().fill(Color.clear))
        })
    }
}

// MARK: - Basic API: Decoders

protocol GraphQLValueDecoder {
    associatedtype Encoded
    associatedtype Decoded

    static func decode(encoded: Encoded) throws -> Decoded
}

enum NoOpDecoder<T>: GraphQLValueDecoder {
    static func decode(encoded: T) throws -> T {
        return encoded
    }
}

// MARK: - Basic API: Scalar Handling

protocol GraphQLScalar {
    associatedtype Scalar
    static var placeholder: Self { get }
    init(from scalar: Scalar) throws
}

extension Array: GraphQLScalar where Element: GraphQLScalar {
    static var placeholder: [Element] {
        return Array(repeating: Element.placeholder, count: 5)
    }

    init(from scalar: [Element.Scalar]) throws {
        self = try scalar.map { try Element(from: $0) }
    }
}

extension Optional: GraphQLScalar where Wrapped: GraphQLScalar {
    static var placeholder: Wrapped? {
        return Wrapped.placeholder
    }

    init(from scalar: Wrapped.Scalar?) throws {
        guard let scalar = scalar else {
            self = .none
            return
        }
        self = .some(try Wrapped(from: scalar))
    }
}

extension URL: GraphQLScalar {
    typealias Scalar = String

    static let placeholder: URL = URL(string: "https://graphaello.dev/assets/logo.png")!

    private struct URLScalarDecodingError: Error {
        let string: String
    }

    init(from string: Scalar) throws {
        guard let url = URL(string: string) else {
            throw URLScalarDecodingError(string: string)
        }
        self = url
    }
}

enum ScalarDecoder<ScalarType: GraphQLScalar>: GraphQLValueDecoder {
    typealias Encoded = ScalarType.Scalar
    typealias Decoded = ScalarType

    static func decode(encoded: ScalarType.Scalar) throws -> ScalarType {
        if let encoded = encoded as? String, encoded == "__GRAPHAELLO_PLACEHOLDER__" {
            return Decoded.placeholder
        }
        return try ScalarType(from: encoded)
    }
}

// MARK: - Basic API: HACK - AnyObservableObject

private class AnyObservableObject: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var cancellable: AnyCancellable?

    func use<O: ObservableObject>(_ object: O) {
        cancellable?.cancel()
        cancellable = object.objectWillChange.sink { [unowned self] _ in self.objectWillChange.send() }
    }
}

// MARK: - Basic API: Graph QL Property Wrapper

@propertyWrapper
struct GraphQL<Decoder: GraphQLValueDecoder>: DynamicProperty {
    private let initialValue: Decoder.Decoded

    @State
    private var value: Decoder.Decoded? = nil

    @ObservedObject
    private var observed: AnyObservableObject = AnyObservableObject()
    private let updateObserved: ((Decoder.Decoded) -> Void)?

    var wrappedValue: Decoder.Decoded {
        get {
            return value ?? initialValue
        }
        nonmutating set {
            value = newValue
            updateObserved?(newValue)
        }
    }

    var projectedValue: Binding<Decoder.Decoded> {
        return Binding(get: { self.wrappedValue }, set: { newValue in self.wrappedValue = newValue })
    }

    init<T: Target>(_: @autoclosure () -> GraphQLPath<T, Decoder.Encoded>) {
        fatalError("Initializer with path only should never be used")
    }

    init<T: Target, Value>(_: @autoclosure () -> GraphQLPath<T, Value>) where Decoder == NoOpDecoder<Value> {
        fatalError("Initializer with path only should never be used")
    }

    init<T: Target, Value: GraphQLScalar>(_: @autoclosure () -> GraphQLPath<T, Value.Scalar>) where Decoder == ScalarDecoder<Value> {
        fatalError("Initializer with path only should never be used")
    }

    fileprivate init(_ wrappedValue: Decoder.Encoded) {
        initialValue = try! Decoder.decode(encoded: wrappedValue)
        updateObserved = nil
    }

    mutating func update() {
        _value.update()
        _observed.update()
    }
}

extension GraphQL where Decoder.Decoded: ObservableObject {
    fileprivate init(_ wrappedValue: Decoder.Encoded) {
        let value = try! Decoder.decode(encoded: wrappedValue)
        initialValue = value

        let observed = AnyObservableObject()
        observed.use(value)

        self.observed = observed
        updateObserved = { observed.use($0) }
    }
}

extension GraphQL {
    init<T: Target, Value: Fragment>(_: @autoclosure () -> GraphQLFragmentPath<T, Value.UnderlyingType>) where Decoder == NoOpDecoder<Value> {
        fatalError("Initializer with path only should never be used")
    }
}

extension GraphQL {
    init<T: API, C: Connection, F: Fragment>(_: @autoclosure () -> GraphQLFragmentPath<T, C>) where Decoder == NoOpDecoder<Paging<F>>, C.Node == F.UnderlyingType {
        fatalError("Initializer with path only should never be used")
    }

    init<T: API, C: Connection, F: Fragment>(_: @autoclosure () -> GraphQLFragmentPath<T, C?>) where Decoder == NoOpDecoder<Paging<F>?>, C.Node == F.UnderlyingType {
        fatalError("Initializer with path only should never be used")
    }
}

extension GraphQL {
    init<T: MutationTarget, MutationType: Mutation>(_: @autoclosure () -> GraphQLPath<T, MutationType.Value>) where Decoder == NoOpDecoder<MutationType> {
        fatalError("Initializer with path only should never be used")
    }

    init<T: MutationTarget, MutationType: Mutation>(_: @autoclosure () -> GraphQLFragmentPath<T, MutationType.Value.UnderlyingType>) where Decoder == NoOpDecoder<MutationType>, MutationType.Value: Fragment {
        fatalError("Initializer with path only should never be used")
    }
}

extension GraphQL {
    init<T: Target, M: MutationTarget, MutationType: CurrentValueMutation>(_: @autoclosure () -> GraphQLPath<T, MutationType.Value>, mutation _: @autoclosure () -> GraphQLPath<M, MutationType.Value>) where Decoder == NoOpDecoder<MutationType> {
        fatalError("Initializer with path only should never be used")
    }

    init<T: Target, M: MutationTarget, MutationType: CurrentValueMutation>(_: @autoclosure () -> GraphQLFragmentPath<T, MutationType.Value.UnderlyingType>, mutation _: @autoclosure () -> GraphQLFragmentPath<M, MutationType.Value.UnderlyingType>) where Decoder == NoOpDecoder<MutationType>, MutationType.Value: Fragment {
        fatalError("Initializer with path only should never be used")
    }
}


// MARK: - ProductHunt

#if GRAPHAELLO_PRODUCT_HUNT_PLAYGROUND_TARGET

    struct ProductHunt: API {
        let client: ApolloClient

        typealias Query = ProductHunt
        typealias Path<V> = GraphQLPath<ProductHunt, V>
        typealias FragmentPath<V> = GraphQLFragmentPath<ProductHunt, V>

        enum Mutation: MutationTarget {
            typealias Path<V> = GraphQLPath<ProductHunt.Mutation, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<ProductHunt.Mutation, V>

            static func goalCheer(input _: GraphQLArgument<ProductHunt.GoalCheerInput> = .argument) -> FragmentPath<ProductHunt.GoalCheerPayload> {
                return .init()
            }

            static var goalCheer: FragmentPath<ProductHunt.GoalCheerPayload> { .init() }

            static func goalCheerUndo(input _: GraphQLArgument<ProductHunt.GoalCheerUndoInput> = .argument) -> FragmentPath<ProductHunt.GoalCheerUndoPayload> {
                return .init()
            }

            static var goalCheerUndo: FragmentPath<ProductHunt.GoalCheerUndoPayload> { .init() }

            static func goalCreate(input _: GraphQLArgument<ProductHunt.GoalCreateInput> = .argument) -> FragmentPath<ProductHunt.GoalCreatePayload> {
                return .init()
            }

            static var goalCreate: FragmentPath<ProductHunt.GoalCreatePayload> { .init() }

            static func goalMarkAsComplete(input _: GraphQLArgument<ProductHunt.GoalMarkAsCompleteInput> = .argument) -> FragmentPath<ProductHunt.GoalMarkAsCompletePayload> {
                return .init()
            }

            static var goalMarkAsComplete: FragmentPath<ProductHunt.GoalMarkAsCompletePayload> { .init() }

            static func goalMarkAsIncomplete(input _: GraphQLArgument<ProductHunt.GoalMarkAsIncompleteInput> = .argument) -> FragmentPath<ProductHunt.GoalMarkAsIncompletePayload> {
                return .init()
            }

            static var goalMarkAsIncomplete: FragmentPath<ProductHunt.GoalMarkAsIncompletePayload> { .init() }

            static func goalUpdate(input _: GraphQLArgument<ProductHunt.GoalUpdateInput> = .argument) -> FragmentPath<ProductHunt.GoalUpdatePayload> {
                return .init()
            }

            static var goalUpdate: FragmentPath<ProductHunt.GoalUpdatePayload> { .init() }

            static func userFollow(input _: GraphQLArgument<ProductHunt.UserFollowInput> = .argument) -> FragmentPath<ProductHunt.UserFollowPayload> {
                return .init()
            }

            static var userFollow: FragmentPath<ProductHunt.UserFollowPayload> { .init() }

            static func userFollowUndo(input _: GraphQLArgument<ProductHunt.UserFollowUndoInput> = .argument) -> FragmentPath<ProductHunt.UserFollowUndoPayload> {
                return .init()
            }

            static var userFollowUndo: FragmentPath<ProductHunt.UserFollowUndoPayload> { .init() }
        }

        static func collection(id _: GraphQLArgument<String?> = .argument,
                               slug _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.Collection?> {
            return .init()
        }

        static var collection: FragmentPath<ProductHunt.Collection?> { .init() }

        static func collections(first _: GraphQLArgument<Int?> = .argument,
                                after _: GraphQLArgument<String?> = .argument,
                                last _: GraphQLArgument<Int?> = .argument,
                                before _: GraphQLArgument<String?> = .argument,
                                postId _: GraphQLArgument<String?> = .argument,
                                userId _: GraphQLArgument<String?> = .argument,
                                featured _: GraphQLArgument<Bool?> = .argument,
                                order _: GraphQLArgument<ProductHunt.CollectionsOrder?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection> {
            return .init()
        }

        static var collections: FragmentPath<ProductHunt.CollectionConnection> { .init() }

        static func comment(id _: GraphQLArgument<String> = .argument) -> FragmentPath<ProductHunt.Comment?> {
            return .init()
        }

        static var comment: FragmentPath<ProductHunt.Comment?> { .init() }

        static func goal(id _: GraphQLArgument<String> = .argument) -> FragmentPath<ProductHunt.Goal?> {
            return .init()
        }

        static var goal: FragmentPath<ProductHunt.Goal?> { .init() }

        static func goals(first _: GraphQLArgument<Int?> = .argument,
                          after _: GraphQLArgument<String?> = .argument,
                          last _: GraphQLArgument<Int?> = .argument,
                          before _: GraphQLArgument<String?> = .argument,
                          userId _: GraphQLArgument<String?> = .argument,
                          makerGroupId _: GraphQLArgument<String?> = .argument,
                          makerProjectId _: GraphQLArgument<String?> = .argument,
                          completed _: GraphQLArgument<Bool?> = .argument,
                          order _: GraphQLArgument<ProductHunt.GoalsOrder?> = .argument) -> FragmentPath<ProductHunt.GoalConnection> {
            return .init()
        }

        static var goals: FragmentPath<ProductHunt.GoalConnection> { .init() }

        static func makerGroup(id _: GraphQLArgument<String> = .argument) -> FragmentPath<ProductHunt.MakerGroup?> {
            return .init()
        }

        static var makerGroup: FragmentPath<ProductHunt.MakerGroup?> { .init() }

        static func makerGroups(first _: GraphQLArgument<Int?> = .argument,
                                after _: GraphQLArgument<String?> = .argument,
                                last _: GraphQLArgument<Int?> = .argument,
                                before _: GraphQLArgument<String?> = .argument,
                                userId _: GraphQLArgument<String?> = .argument,
                                order _: GraphQLArgument<ProductHunt.MakerGroupsOrder?> = .argument) -> FragmentPath<ProductHunt.MakerGroupConnection> {
            return .init()
        }

        static var makerGroups: FragmentPath<ProductHunt.MakerGroupConnection> { .init() }

        static func post(id _: GraphQLArgument<String?> = .argument,
                         slug _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.Post?> {
            return .init()
        }

        static var post: FragmentPath<ProductHunt.Post?> { .init() }

        static func posts(first _: GraphQLArgument<Int?> = .argument,
                          after _: GraphQLArgument<String?> = .argument,
                          last _: GraphQLArgument<Int?> = .argument,
                          before _: GraphQLArgument<String?> = .argument,
                          featured _: GraphQLArgument<Bool?> = .argument,
                          postedBefore _: GraphQLArgument<String?> = .argument,
                          postedAfter _: GraphQLArgument<String?> = .argument,
                          topic _: GraphQLArgument<String?> = .argument,
                          order _: GraphQLArgument<ProductHunt.PostsOrder?> = .argument,
                          twitterUrl _: GraphQLArgument<String?> = .argument,
                          url _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
            return .init()
        }

        static var posts: FragmentPath<ProductHunt.PostConnection> { .init() }

        static func topic(id _: GraphQLArgument<String?> = .argument,
                          slug _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.Topic?> {
            return .init()
        }

        static var topic: FragmentPath<ProductHunt.Topic?> { .init() }

        static func topics(first _: GraphQLArgument<Int?> = .argument,
                           after _: GraphQLArgument<String?> = .argument,
                           last _: GraphQLArgument<Int?> = .argument,
                           before _: GraphQLArgument<String?> = .argument,
                           followedByUserId _: GraphQLArgument<String?> = .argument,
                           query _: GraphQLArgument<String?> = .argument,
                           order _: GraphQLArgument<ProductHunt.TopicsOrder?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
            return .init()
        }

        static var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

        static func user(id _: GraphQLArgument<String?> = .argument,
                         username _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.User?> {
            return .init()
        }

        static var user: FragmentPath<ProductHunt.User?> { .init() }

        static var viewer: FragmentPath<ProductHunt.Viewer?> { .init() }

        enum Collection: Target {
            typealias Path<V> = GraphQLPath<Collection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Collection, V>

            static func coverImage(width _: GraphQLArgument<Int?> = .argument,
                                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
                return .init()
            }

            static var coverImage: Path<String?> { .init() }

            static var createdAt: Path<String> { .init() }

            static var description: Path<String?> { .init() }

            static var featuredAt: Path<String?> { .init() }

            static var followersCount: Path<Int> { .init() }

            static var id: Path<String> { .init() }

            static var isFollowing: Path<Bool> { .init() }

            static var name: Path<String> { .init() }

            static func posts(first _: GraphQLArgument<Int?> = .argument,
                              after _: GraphQLArgument<String?> = .argument,
                              last _: GraphQLArgument<Int?> = .argument,
                              before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
                return .init()
            }

            static var posts: FragmentPath<ProductHunt.PostConnection> { .init() }

            static var tagline: Path<String> { .init() }

            static func topics(first _: GraphQLArgument<Int?> = .argument,
                               after _: GraphQLArgument<String?> = .argument,
                               last _: GraphQLArgument<Int?> = .argument,
                               before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
                return .init()
            }

            static var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

            static var url: Path<String> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var userId: Path<String> { .init() }

            static var topicableInterface: FragmentPath<TopicableInterface> { .init() }

            static var _fragment: FragmentPath<Collection> { .init() }
        }

        enum TopicableInterface: Target {
            typealias Path<V> = GraphQLPath<TopicableInterface, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<TopicableInterface, V>

            static var id: Path<String> { .init() }

            static func topics(first _: GraphQLArgument<Int?> = .argument,
                               after _: GraphQLArgument<String?> = .argument,
                               last _: GraphQLArgument<Int?> = .argument,
                               before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
                return .init()
            }

            static var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

            static var collection: FragmentPath<Collection?> { .init() }

            static var post: FragmentPath<Post?> { .init() }

            static var _fragment: FragmentPath<TopicableInterface> { .init() }
        }

        enum TopicConnection: Target, Connection {
            typealias Node = ProductHunt.Topic
            typealias Path<V> = GraphQLPath<TopicConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<TopicConnection, V>

            static var edges: FragmentPath<[ProductHunt.TopicEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<TopicConnection> { .init() }
        }

        enum TopicEdge: Target {
            typealias Path<V> = GraphQLPath<TopicEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<TopicEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Topic> { .init() }

            static var _fragment: FragmentPath<TopicEdge> { .init() }
        }

        enum Topic: Target {
            typealias Path<V> = GraphQLPath<Topic, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Topic, V>

            static var createdAt: Path<String> { .init() }

            static var description: Path<String> { .init() }

            static var followersCount: Path<Int> { .init() }

            static var id: Path<String> { .init() }

            static func image(width _: GraphQLArgument<Int?> = .argument,
                              height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
                return .init()
            }

            static var image: Path<String?> { .init() }

            static var isFollowing: Path<Bool> { .init() }

            static var name: Path<String> { .init() }

            static var postsCount: Path<Int> { .init() }

            static var slug: Path<String> { .init() }

            static var url: Path<String> { .init() }

            static var _fragment: FragmentPath<Topic> { .init() }
        }

        enum PageInfo: Target {
            typealias Path<V> = GraphQLPath<PageInfo, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<PageInfo, V>

            static var endCursor: Path<String?> { .init() }

            static var hasNextPage: Path<Bool> { .init() }

            static var hasPreviousPage: Path<Bool> { .init() }

            static var startCursor: Path<String?> { .init() }

            static var _fragment: FragmentPath<PageInfo> { .init() }
        }

        enum PostConnection: Target, Connection {
            typealias Node = ProductHunt.Post
            typealias Path<V> = GraphQLPath<PostConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<PostConnection, V>

            static var edges: FragmentPath<[ProductHunt.PostEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<PostConnection> { .init() }
        }

        enum PostEdge: Target {
            typealias Path<V> = GraphQLPath<PostEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<PostEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Post> { .init() }

            static var _fragment: FragmentPath<PostEdge> { .init() }
        }

        enum Post: Target {
            typealias Path<V> = GraphQLPath<Post, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Post, V>

            static func collections(first _: GraphQLArgument<Int?> = .argument,
                                    after _: GraphQLArgument<String?> = .argument,
                                    last _: GraphQLArgument<Int?> = .argument,
                                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection> {
                return .init()
            }

            static var collections: FragmentPath<ProductHunt.CollectionConnection> { .init() }

            static func comments(first _: GraphQLArgument<Int?> = .argument,
                                 after _: GraphQLArgument<String?> = .argument,
                                 last _: GraphQLArgument<Int?> = .argument,
                                 before _: GraphQLArgument<String?> = .argument,
                                 order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection> {
                return .init()
            }

            static var comments: FragmentPath<ProductHunt.CommentConnection> { .init() }

            static var commentsCount: Path<Int> { .init() }

            static var createdAt: Path<String> { .init() }

            static var description: Path<String?> { .init() }

            static var featuredAt: Path<String?> { .init() }

            static var id: Path<String> { .init() }

            static var isCollected: Path<Bool> { .init() }

            static var isVoted: Path<Bool> { .init() }

            static var makers: FragmentPath<[ProductHunt.User]> { .init() }

            static var media: FragmentPath<[ProductHunt.Media]> { .init() }

            static var name: Path<String> { .init() }

            static var productLinks: FragmentPath<[ProductHunt.ProductLink]> { .init() }

            static var reviewsCount: Path<Int> { .init() }

            static var reviewsRating: Path<Double> { .init() }

            static var slug: Path<String> { .init() }

            static var tagline: Path<String> { .init() }

            static var thumbnail: FragmentPath<ProductHunt.Media?> { .init() }

            static func topics(first _: GraphQLArgument<Int?> = .argument,
                               after _: GraphQLArgument<String?> = .argument,
                               last _: GraphQLArgument<Int?> = .argument,
                               before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
                return .init()
            }

            static var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

            static var url: Path<String> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var userId: Path<String> { .init() }

            static func votes(first _: GraphQLArgument<Int?> = .argument,
                              after _: GraphQLArgument<String?> = .argument,
                              last _: GraphQLArgument<Int?> = .argument,
                              before _: GraphQLArgument<String?> = .argument,
                              createdAfter _: GraphQLArgument<String?> = .argument,
                              createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
                return .init()
            }

            static var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

            static var votesCount: Path<Int> { .init() }

            static var website: Path<String> { .init() }

            static var votableInterface: FragmentPath<VotableInterface> { .init() }

            static var topicableInterface: FragmentPath<TopicableInterface> { .init() }

            static var _fragment: FragmentPath<Post> { .init() }
        }

        enum VotableInterface: Target {
            typealias Path<V> = GraphQLPath<VotableInterface, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<VotableInterface, V>

            static var id: Path<String> { .init() }

            static var isVoted: Path<Bool> { .init() }

            static func votes(first _: GraphQLArgument<Int?> = .argument,
                              after _: GraphQLArgument<String?> = .argument,
                              last _: GraphQLArgument<Int?> = .argument,
                              before _: GraphQLArgument<String?> = .argument,
                              createdAfter _: GraphQLArgument<String?> = .argument,
                              createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
                return .init()
            }

            static var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

            static var votesCount: Path<Int> { .init() }

            static var post: FragmentPath<Post?> { .init() }

            static var comment: FragmentPath<Comment?> { .init() }

            static var _fragment: FragmentPath<VotableInterface> { .init() }
        }

        enum VoteConnection: Target, Connection {
            typealias Node = ProductHunt.Vote
            typealias Path<V> = GraphQLPath<VoteConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<VoteConnection, V>

            static var edges: FragmentPath<[ProductHunt.VoteEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<VoteConnection> { .init() }
        }

        enum VoteEdge: Target {
            typealias Path<V> = GraphQLPath<VoteEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<VoteEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Vote> { .init() }

            static var _fragment: FragmentPath<VoteEdge> { .init() }
        }

        enum Vote: Target {
            typealias Path<V> = GraphQLPath<Vote, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Vote, V>

            static var createdAt: Path<String> { .init() }

            static var id: Path<String> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var userId: Path<String> { .init() }

            static var _fragment: FragmentPath<Vote> { .init() }
        }

        enum User: Target {
            typealias Path<V> = GraphQLPath<User, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<User, V>

            static func coverImage(width _: GraphQLArgument<Int?> = .argument,
                                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
                return .init()
            }

            static var coverImage: Path<String?> { .init() }

            static var createdAt: Path<String> { .init() }

            static func followedCollections(first _: GraphQLArgument<Int?> = .argument,
                                            after _: GraphQLArgument<String?> = .argument,
                                            last _: GraphQLArgument<Int?> = .argument,
                                            before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection> {
                return .init()
            }

            static var followedCollections: FragmentPath<ProductHunt.CollectionConnection> { .init() }

            static func followers(first _: GraphQLArgument<Int?> = .argument,
                                  after _: GraphQLArgument<String?> = .argument,
                                  last _: GraphQLArgument<Int?> = .argument,
                                  before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection> {
                return .init()
            }

            static var followers: FragmentPath<ProductHunt.UserConnection> { .init() }

            static func following(first _: GraphQLArgument<Int?> = .argument,
                                  after _: GraphQLArgument<String?> = .argument,
                                  last _: GraphQLArgument<Int?> = .argument,
                                  before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection> {
                return .init()
            }

            static var following: FragmentPath<ProductHunt.UserConnection> { .init() }

            static var headline: Path<String?> { .init() }

            static var id: Path<String> { .init() }

            static var isFollowing: Path<Bool> { .init() }

            static var isMaker: Path<Bool> { .init() }

            static var isViewer: Path<Bool> { .init() }

            static func madePosts(first _: GraphQLArgument<Int?> = .argument,
                                  after _: GraphQLArgument<String?> = .argument,
                                  last _: GraphQLArgument<Int?> = .argument,
                                  before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
                return .init()
            }

            static var madePosts: FragmentPath<ProductHunt.PostConnection> { .init() }

            static var name: Path<String> { .init() }

            static func profileImage(size _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
                return .init()
            }

            static var profileImage: Path<String?> { .init() }

            static func submittedPosts(first _: GraphQLArgument<Int?> = .argument,
                                       after _: GraphQLArgument<String?> = .argument,
                                       last _: GraphQLArgument<Int?> = .argument,
                                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
                return .init()
            }

            static var submittedPosts: FragmentPath<ProductHunt.PostConnection> { .init() }

            static var twitterUsername: Path<String?> { .init() }

            static var url: Path<String> { .init() }

            static var username: Path<String> { .init() }

            static func votedPosts(first _: GraphQLArgument<Int?> = .argument,
                                   after _: GraphQLArgument<String?> = .argument,
                                   last _: GraphQLArgument<Int?> = .argument,
                                   before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
                return .init()
            }

            static var votedPosts: FragmentPath<ProductHunt.PostConnection> { .init() }

            static var websiteUrl: Path<String?> { .init() }

            static var _fragment: FragmentPath<User> { .init() }
        }

        enum CollectionConnection: Target, Connection {
            typealias Node = ProductHunt.Collection
            typealias Path<V> = GraphQLPath<CollectionConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CollectionConnection, V>

            static var edges: FragmentPath<[ProductHunt.CollectionEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<CollectionConnection> { .init() }
        }

        enum CollectionEdge: Target {
            typealias Path<V> = GraphQLPath<CollectionEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CollectionEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Collection> { .init() }

            static var _fragment: FragmentPath<CollectionEdge> { .init() }
        }

        enum UserConnection: Target, Connection {
            typealias Node = ProductHunt.User
            typealias Path<V> = GraphQLPath<UserConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserConnection, V>

            static var edges: FragmentPath<[ProductHunt.UserEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<UserConnection> { .init() }
        }

        enum UserEdge: Target {
            typealias Path<V> = GraphQLPath<UserEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.User> { .init() }

            static var _fragment: FragmentPath<UserEdge> { .init() }
        }

        enum CommentsOrder: String, Target {
            typealias Path<V> = GraphQLPath<CommentsOrder, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CommentsOrder, V>

            case newest = "NEWEST"

            case votes_count = "VOTES_COUNT"

            static var _fragment: FragmentPath<CommentsOrder> { .init() }
        }

        enum CommentConnection: Target, Connection {
            typealias Node = ProductHunt.Comment
            typealias Path<V> = GraphQLPath<CommentConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CommentConnection, V>

            static var edges: FragmentPath<[ProductHunt.CommentEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<CommentConnection> { .init() }
        }

        enum CommentEdge: Target {
            typealias Path<V> = GraphQLPath<CommentEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CommentEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Comment> { .init() }

            static var _fragment: FragmentPath<CommentEdge> { .init() }
        }

        enum Comment: Target {
            typealias Path<V> = GraphQLPath<Comment, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Comment, V>

            static var body: Path<String> { .init() }

            static var createdAt: Path<String> { .init() }

            static var id: Path<String> { .init() }

            static var isVoted: Path<Bool> { .init() }

            static var parent: FragmentPath<ProductHunt.Comment?> { .init() }

            static var parentId: Path<String?> { .init() }

            static func replies(first _: GraphQLArgument<Int?> = .argument,
                                after _: GraphQLArgument<String?> = .argument,
                                last _: GraphQLArgument<Int?> = .argument,
                                before _: GraphQLArgument<String?> = .argument,
                                order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection> {
                return .init()
            }

            static var replies: FragmentPath<ProductHunt.CommentConnection> { .init() }

            static var url: Path<String> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var userId: Path<String> { .init() }

            static func votes(first _: GraphQLArgument<Int?> = .argument,
                              after _: GraphQLArgument<String?> = .argument,
                              last _: GraphQLArgument<Int?> = .argument,
                              before _: GraphQLArgument<String?> = .argument,
                              createdAfter _: GraphQLArgument<String?> = .argument,
                              createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
                return .init()
            }

            static var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

            static var votesCount: Path<Int> { .init() }

            static var votableInterface: FragmentPath<VotableInterface> { .init() }

            static var _fragment: FragmentPath<Comment> { .init() }
        }

        enum Media: Target {
            typealias Path<V> = GraphQLPath<Media, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Media, V>

            static var type: Path<String> { .init() }

            static func url(width _: GraphQLArgument<Int?> = .argument,
                            height _: GraphQLArgument<Int?> = .argument) -> Path<String> {
                return .init()
            }

            static var url: Path<String> { .init() }

            static var videoUrl: Path<String?> { .init() }

            static var _fragment: FragmentPath<Media> { .init() }
        }

        enum ProductLink: Target {
            typealias Path<V> = GraphQLPath<ProductLink, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<ProductLink, V>

            static var type: Path<String> { .init() }

            static var url: Path<String> { .init() }

            static var _fragment: FragmentPath<ProductLink> { .init() }
        }

        enum CollectionsOrder: String, Target {
            typealias Path<V> = GraphQLPath<CollectionsOrder, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<CollectionsOrder, V>

            case newest = "NEWEST"

            case followers_count = "FOLLOWERS_COUNT"

            case featured_at = "FEATURED_AT"

            static var _fragment: FragmentPath<CollectionsOrder> { .init() }
        }

        enum Goal: Target {
            typealias Path<V> = GraphQLPath<Goal, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Goal, V>

            static var cheerCount: Path<Int> { .init() }

            static var completedAt: Path<String?> { .init() }

            static var createdAt: Path<String> { .init() }

            static var current: Path<Bool> { .init() }

            static var currentUntil: Path<String?> { .init() }

            static var dueAt: Path<String?> { .init() }

            static var focusedDuration: Path<Int> { .init() }

            static var group: FragmentPath<ProductHunt.MakerGroup> { .init() }

            static var groupId: Path<String> { .init() }

            static var id: Path<String> { .init() }

            static var isCheered: Path<Bool> { .init() }

            static var project: FragmentPath<ProductHunt.MakerProject?> { .init() }

            static var title: Path<String> { .init() }

            static var url: Path<String> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var userId: Path<String> { .init() }

            static var _fragment: FragmentPath<Goal> { .init() }
        }

        enum MakerGroup: Target {
            typealias Path<V> = GraphQLPath<MakerGroup, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerGroup, V>

            static var description: Path<String> { .init() }

            static var goalsCount: Path<Int> { .init() }

            static var id: Path<String> { .init() }

            static var isMember: Path<Bool> { .init() }

            static var membersCount: Path<Int> { .init() }

            static var name: Path<String> { .init() }

            static var tagline: Path<String> { .init() }

            static var url: Path<String> { .init() }

            static var _fragment: FragmentPath<MakerGroup> { .init() }
        }

        enum MakerProject: Target {
            typealias Path<V> = GraphQLPath<MakerProject, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerProject, V>

            static var id: Path<String> { .init() }

            static func image(width _: GraphQLArgument<Int?> = .argument,
                              height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
                return .init()
            }

            static var image: Path<String?> { .init() }

            static var lookingForOtherMakers: Path<Bool> { .init() }

            static var name: Path<String> { .init() }

            static var tagline: Path<String> { .init() }

            static var url: Path<String> { .init() }

            static var _fragment: FragmentPath<MakerProject> { .init() }
        }

        enum GoalsOrder: String, Target {
            typealias Path<V> = GraphQLPath<GoalsOrder, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalsOrder, V>

            case completed_at = "COMPLETED_AT"

            case due_at = "DUE_AT"

            case newest = "NEWEST"

            static var _fragment: FragmentPath<GoalsOrder> { .init() }
        }

        enum GoalConnection: Target, Connection {
            typealias Node = ProductHunt.Goal
            typealias Path<V> = GraphQLPath<GoalConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalConnection, V>

            static var edges: FragmentPath<[ProductHunt.GoalEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<GoalConnection> { .init() }
        }

        enum GoalEdge: Target {
            typealias Path<V> = GraphQLPath<GoalEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.Goal> { .init() }

            static var _fragment: FragmentPath<GoalEdge> { .init() }
        }

        enum MakerGroupsOrder: String, Target {
            typealias Path<V> = GraphQLPath<MakerGroupsOrder, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerGroupsOrder, V>

            case last_active = "LAST_ACTIVE"

            case members_count = "MEMBERS_COUNT"

            case goals_count = "GOALS_COUNT"

            case newest = "NEWEST"

            static var _fragment: FragmentPath<MakerGroupsOrder> { .init() }
        }

        enum MakerGroupConnection: Target, Connection {
            typealias Node = ProductHunt.MakerGroup
            typealias Path<V> = GraphQLPath<MakerGroupConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerGroupConnection, V>

            static var edges: FragmentPath<[ProductHunt.MakerGroupEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<MakerGroupConnection> { .init() }
        }

        enum MakerGroupEdge: Target {
            typealias Path<V> = GraphQLPath<MakerGroupEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerGroupEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.MakerGroup> { .init() }

            static var _fragment: FragmentPath<MakerGroupEdge> { .init() }
        }

        typealias PostsOrder = ApolloProductHunt.PostsOrder

        enum TopicsOrder: String, Target {
            typealias Path<V> = GraphQLPath<TopicsOrder, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<TopicsOrder, V>

            case newest = "NEWEST"

            case followers_count = "FOLLOWERS_COUNT"

            static var _fragment: FragmentPath<TopicsOrder> { .init() }
        }

        enum Viewer: Target {
            typealias Path<V> = GraphQLPath<Viewer, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Viewer, V>

            static func goals(first _: GraphQLArgument<Int?> = .argument,
                              after _: GraphQLArgument<String?> = .argument,
                              last _: GraphQLArgument<Int?> = .argument,
                              before _: GraphQLArgument<String?> = .argument,
                              current _: GraphQLArgument<Bool?> = .argument,
                              order _: GraphQLArgument<ProductHunt.GoalsOrder?> = .argument) -> FragmentPath<ProductHunt.GoalConnection> {
                return .init()
            }

            static var goals: FragmentPath<ProductHunt.GoalConnection> { .init() }

            static func makerGroups(first _: GraphQLArgument<Int?> = .argument,
                                    after _: GraphQLArgument<String?> = .argument,
                                    last _: GraphQLArgument<Int?> = .argument,
                                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerGroupConnection> {
                return .init()
            }

            static var makerGroups: FragmentPath<ProductHunt.MakerGroupConnection> { .init() }

            static func makerProjects(first _: GraphQLArgument<Int?> = .argument,
                                      after _: GraphQLArgument<String?> = .argument,
                                      last _: GraphQLArgument<Int?> = .argument,
                                      before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerProjectConnection> {
                return .init()
            }

            static var makerProjects: FragmentPath<ProductHunt.MakerProjectConnection> { .init() }

            static var user: FragmentPath<ProductHunt.User> { .init() }

            static var _fragment: FragmentPath<Viewer> { .init() }
        }

        enum MakerProjectConnection: Target, Connection {
            typealias Node = ProductHunt.MakerProject
            typealias Path<V> = GraphQLPath<MakerProjectConnection, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerProjectConnection, V>

            static var edges: FragmentPath<[ProductHunt.MakerProjectEdge]> { .init() }

            static var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

            static var totalCount: Path<Int> { .init() }

            static var _fragment: FragmentPath<MakerProjectConnection> { .init() }
        }

        enum MakerProjectEdge: Target {
            typealias Path<V> = GraphQLPath<MakerProjectEdge, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<MakerProjectEdge, V>

            static var cursor: Path<String> { .init() }

            static var node: FragmentPath<ProductHunt.MakerProject> { .init() }

            static var _fragment: FragmentPath<MakerProjectEdge> { .init() }
        }

        struct GoalCheerInput: Target {
            typealias Path<V> = GraphQLPath<GoalCheerInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCheerInput, V>

            init(goalId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalCheerInput> { .init() }
        }

        enum GoalCheerPayload: Target {
            typealias Path<V> = GraphQLPath<GoalCheerPayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCheerPayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalCheerPayload> { .init() }
        }

        enum Error: Target {
            typealias Path<V> = GraphQLPath<Error, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<Error, V>

            static var field: Path<String> { .init() }

            static var message: Path<String> { .init() }

            static var _fragment: FragmentPath<Error> { .init() }
        }

        struct GoalCheerUndoInput: Target {
            typealias Path<V> = GraphQLPath<GoalCheerUndoInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCheerUndoInput, V>

            init(goalId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalCheerUndoInput> { .init() }
        }

        enum GoalCheerUndoPayload: Target {
            typealias Path<V> = GraphQLPath<GoalCheerUndoPayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCheerUndoPayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalCheerUndoPayload> { .init() }
        }

        struct GoalCreateInput: Target {
            typealias Path<V> = GraphQLPath<GoalCreateInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCreateInput, V>

            init(projectId _: String? = nil,
                 groupId _: String? = nil,
                 dueAt _: String? = nil,
                 title _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalCreateInput> { .init() }
        }

        enum GoalCreatePayload: Target {
            typealias Path<V> = GraphQLPath<GoalCreatePayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalCreatePayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalCreatePayload> { .init() }
        }

        struct GoalMarkAsCompleteInput: Target {
            typealias Path<V> = GraphQLPath<GoalMarkAsCompleteInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalMarkAsCompleteInput, V>

            init(goalId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalMarkAsCompleteInput> { .init() }
        }

        enum GoalMarkAsCompletePayload: Target {
            typealias Path<V> = GraphQLPath<GoalMarkAsCompletePayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalMarkAsCompletePayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalMarkAsCompletePayload> { .init() }
        }

        struct GoalMarkAsIncompleteInput: Target {
            typealias Path<V> = GraphQLPath<GoalMarkAsIncompleteInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalMarkAsIncompleteInput, V>

            init(goalId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalMarkAsIncompleteInput> { .init() }
        }

        enum GoalMarkAsIncompletePayload: Target {
            typealias Path<V> = GraphQLPath<GoalMarkAsIncompletePayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalMarkAsIncompletePayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalMarkAsIncompletePayload> { .init() }
        }

        struct GoalUpdateInput: Target {
            typealias Path<V> = GraphQLPath<GoalUpdateInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalUpdateInput, V>

            init(goalId _: String,
                 groupId _: String? = nil,
                 dueAt _: String? = nil,
                 title _: String? = nil,
                 projectId _: String? = nil,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<GoalUpdateInput> { .init() }
        }

        enum GoalUpdatePayload: Target {
            typealias Path<V> = GraphQLPath<GoalUpdatePayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<GoalUpdatePayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.Goal?> { .init() }

            static var _fragment: FragmentPath<GoalUpdatePayload> { .init() }
        }

        struct UserFollowInput: Target {
            typealias Path<V> = GraphQLPath<UserFollowInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserFollowInput, V>

            init(userId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<UserFollowInput> { .init() }
        }

        enum UserFollowPayload: Target {
            typealias Path<V> = GraphQLPath<UserFollowPayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserFollowPayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.User?> { .init() }

            static var _fragment: FragmentPath<UserFollowPayload> { .init() }
        }

        struct UserFollowUndoInput: Target {
            typealias Path<V> = GraphQLPath<UserFollowUndoInput, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserFollowUndoInput, V>

            init(userId _: String,
                 clientMutationId _: String? = nil) {
                // no-op
            }

            static var _fragment: FragmentPath<UserFollowUndoInput> { .init() }
        }

        enum UserFollowUndoPayload: Target {
            typealias Path<V> = GraphQLPath<UserFollowUndoPayload, V>
            typealias FragmentPath<V> = GraphQLFragmentPath<UserFollowUndoPayload, V>

            static var clientMutationId: Path<String?> { .init() }

            static var errors: FragmentPath<[ProductHunt.Error]> { .init() }

            static var node: FragmentPath<ProductHunt.User?> { .init() }

            static var _fragment: FragmentPath<UserFollowUndoPayload> { .init() }
        }
    }

    extension ProductHunt {
        init(url: URL,
             client: URLSessionClient = URLSessionClient(),
             useGETForQueries: Bool = false,
             enableAutoPersistedQueries: Bool = false,
             useGETForPersistedQueryRetry: Bool = false,
             requestBodyCreator: RequestBodyCreator = ApolloRequestBodyCreator(),
             store: ApolloStore = ApolloStore(cache: InMemoryNormalizedCache())) {
            let provider = LegacyInterceptorProvider(client: client, store: store)
            let networkTransport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                                endpointURL: url,
                                                                autoPersistQueries: enableAutoPersistedQueries,
                                                                requestBodyCreator: requestBodyCreator,
                                                                useGETForQueries: useGETForQueries,
                                                                useGETForPersistedQueryRetry: useGETForPersistedQueryRetry)
            self.init(client: ApolloClient(networkTransport: networkTransport, store: store))
        }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Collection {
        func coverImage(width _: GraphQLArgument<Int?> = .argument,
                        height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var coverImage: Path<String?> { .init() }

        var createdAt: Path<String> { .init() }

        var description: Path<String?> { .init() }

        var featuredAt: Path<String?> { .init() }

        var followersCount: Path<Int> { .init() }

        var id: Path<String> { .init() }

        var isFollowing: Path<Bool> { .init() }

        var name: Path<String> { .init() }

        func posts(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
            return .init()
        }

        var posts: FragmentPath<ProductHunt.PostConnection> { .init() }

        var tagline: Path<String> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

        var url: Path<String> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }

        var userId: Path<String> { .init() }

        var topicableInterface: FragmentPath<ProductHunt.TopicableInterface> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Collection? {
        func coverImage(width _: GraphQLArgument<Int?> = .argument,
                        height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var coverImage: Path<String?> { .init() }

        var createdAt: Path<String?> { .init() }

        var description: Path<String?> { .init() }

        var featuredAt: Path<String?> { .init() }

        var followersCount: Path<Int?> { .init() }

        var id: Path<String?> { .init() }

        var isFollowing: Path<Bool?> { .init() }

        var name: Path<String?> { .init() }

        func posts(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection?> {
            return .init()
        }

        var posts: FragmentPath<ProductHunt.PostConnection?> { .init() }

        var tagline: Path<String?> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection?> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection?> { .init() }

        var url: Path<String?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }

        var userId: Path<String?> { .init() }

        var topicableInterface: FragmentPath<ProductHunt.TopicableInterface?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicableInterface {
        var id: Path<String> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

        var collection: FragmentPath<ProductHunt.Collection?> { .init() }

        var post: FragmentPath<ProductHunt.Post?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicableInterface? {
        var id: Path<String?> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection?> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection?> { .init() }

        var collection: FragmentPath<ProductHunt.Collection?> { .init() }

        var post: FragmentPath<ProductHunt.Post?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicConnection {
        var edges: FragmentPath<[ProductHunt.TopicEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicConnection? {
        var edges: FragmentPath<[ProductHunt.TopicEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Topic> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Topic?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Topic {
        var createdAt: Path<String> { .init() }

        var description: Path<String> { .init() }

        var followersCount: Path<Int> { .init() }

        var id: Path<String> { .init() }

        func image(width _: GraphQLArgument<Int?> = .argument,
                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var image: Path<String?> { .init() }

        var isFollowing: Path<Bool> { .init() }

        var name: Path<String> { .init() }

        var postsCount: Path<Int> { .init() }

        var slug: Path<String> { .init() }

        var url: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Topic? {
        var createdAt: Path<String?> { .init() }

        var description: Path<String?> { .init() }

        var followersCount: Path<Int?> { .init() }

        var id: Path<String?> { .init() }

        func image(width _: GraphQLArgument<Int?> = .argument,
                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var image: Path<String?> { .init() }

        var isFollowing: Path<Bool?> { .init() }

        var name: Path<String?> { .init() }

        var postsCount: Path<Int?> { .init() }

        var slug: Path<String?> { .init() }

        var url: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PageInfo {
        var endCursor: Path<String?> { .init() }

        var hasNextPage: Path<Bool> { .init() }

        var hasPreviousPage: Path<Bool> { .init() }

        var startCursor: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PageInfo? {
        var endCursor: Path<String?> { .init() }

        var hasNextPage: Path<Bool?> { .init() }

        var hasPreviousPage: Path<Bool?> { .init() }

        var startCursor: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostConnection {
        var edges: FragmentPath<[ProductHunt.PostEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostConnection? {
        var edges: FragmentPath<[ProductHunt.PostEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Post> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Post?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Post {
        func collections(first _: GraphQLArgument<Int?> = .argument,
                         after _: GraphQLArgument<String?> = .argument,
                         last _: GraphQLArgument<Int?> = .argument,
                         before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection> {
            return .init()
        }

        var collections: FragmentPath<ProductHunt.CollectionConnection> { .init() }

        func comments(first _: GraphQLArgument<Int?> = .argument,
                      after _: GraphQLArgument<String?> = .argument,
                      last _: GraphQLArgument<Int?> = .argument,
                      before _: GraphQLArgument<String?> = .argument,
                      order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection> {
            return .init()
        }

        var comments: FragmentPath<ProductHunt.CommentConnection> { .init() }

        var commentsCount: Path<Int> { .init() }

        var createdAt: Path<String> { .init() }

        var description: Path<String?> { .init() }

        var featuredAt: Path<String?> { .init() }

        var id: Path<String> { .init() }

        var isCollected: Path<Bool> { .init() }

        var isVoted: Path<Bool> { .init() }

        var makers: FragmentPath<[ProductHunt.User]> { .init() }

        var media: FragmentPath<[ProductHunt.Media]> { .init() }

        var name: Path<String> { .init() }

        var productLinks: FragmentPath<[ProductHunt.ProductLink]> { .init() }

        var reviewsCount: Path<Int> { .init() }

        var reviewsRating: Path<Double> { .init() }

        var slug: Path<String> { .init() }

        var tagline: Path<String> { .init() }

        var thumbnail: FragmentPath<ProductHunt.Media?> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection> { .init() }

        var url: Path<String> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }

        var userId: Path<String> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

        var votesCount: Path<Int> { .init() }

        var website: Path<String> { .init() }

        var votableInterface: FragmentPath<ProductHunt.VotableInterface> { .init() }

        var topicableInterface: FragmentPath<ProductHunt.TopicableInterface> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Post? {
        func collections(first _: GraphQLArgument<Int?> = .argument,
                         after _: GraphQLArgument<String?> = .argument,
                         last _: GraphQLArgument<Int?> = .argument,
                         before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection?> {
            return .init()
        }

        var collections: FragmentPath<ProductHunt.CollectionConnection?> { .init() }

        func comments(first _: GraphQLArgument<Int?> = .argument,
                      after _: GraphQLArgument<String?> = .argument,
                      last _: GraphQLArgument<Int?> = .argument,
                      before _: GraphQLArgument<String?> = .argument,
                      order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection?> {
            return .init()
        }

        var comments: FragmentPath<ProductHunt.CommentConnection?> { .init() }

        var commentsCount: Path<Int?> { .init() }

        var createdAt: Path<String?> { .init() }

        var description: Path<String?> { .init() }

        var featuredAt: Path<String?> { .init() }

        var id: Path<String?> { .init() }

        var isCollected: Path<Bool?> { .init() }

        var isVoted: Path<Bool?> { .init() }

        var makers: FragmentPath<[ProductHunt.User]?> { .init() }

        var media: FragmentPath<[ProductHunt.Media]?> { .init() }

        var name: Path<String?> { .init() }

        var productLinks: FragmentPath<[ProductHunt.ProductLink]?> { .init() }

        var reviewsCount: Path<Int?> { .init() }

        var reviewsRating: Path<Double?> { .init() }

        var slug: Path<String?> { .init() }

        var tagline: Path<String?> { .init() }

        var thumbnail: FragmentPath<ProductHunt.Media?> { .init() }

        func topics(first _: GraphQLArgument<Int?> = .argument,
                    after _: GraphQLArgument<String?> = .argument,
                    last _: GraphQLArgument<Int?> = .argument,
                    before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.TopicConnection?> {
            return .init()
        }

        var topics: FragmentPath<ProductHunt.TopicConnection?> { .init() }

        var url: Path<String?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }

        var userId: Path<String?> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection?> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection?> { .init() }

        var votesCount: Path<Int?> { .init() }

        var website: Path<String?> { .init() }

        var votableInterface: FragmentPath<ProductHunt.VotableInterface?> { .init() }

        var topicableInterface: FragmentPath<ProductHunt.TopicableInterface?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VotableInterface {
        var id: Path<String> { .init() }

        var isVoted: Path<Bool> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

        var votesCount: Path<Int> { .init() }

        var post: FragmentPath<ProductHunt.Post?> { .init() }

        var comment: FragmentPath<ProductHunt.Comment?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VotableInterface? {
        var id: Path<String?> { .init() }

        var isVoted: Path<Bool?> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection?> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection?> { .init() }

        var votesCount: Path<Int?> { .init() }

        var post: FragmentPath<ProductHunt.Post?> { .init() }

        var comment: FragmentPath<ProductHunt.Comment?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VoteConnection {
        var edges: FragmentPath<[ProductHunt.VoteEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VoteConnection? {
        var edges: FragmentPath<[ProductHunt.VoteEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VoteEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Vote> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.VoteEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Vote?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Vote {
        var createdAt: Path<String> { .init() }

        var id: Path<String> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }

        var userId: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Vote? {
        var createdAt: Path<String?> { .init() }

        var id: Path<String?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }

        var userId: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.User {
        func coverImage(width _: GraphQLArgument<Int?> = .argument,
                        height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var coverImage: Path<String?> { .init() }

        var createdAt: Path<String> { .init() }

        func followedCollections(first _: GraphQLArgument<Int?> = .argument,
                                 after _: GraphQLArgument<String?> = .argument,
                                 last _: GraphQLArgument<Int?> = .argument,
                                 before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection> {
            return .init()
        }

        var followedCollections: FragmentPath<ProductHunt.CollectionConnection> { .init() }

        func followers(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection> {
            return .init()
        }

        var followers: FragmentPath<ProductHunt.UserConnection> { .init() }

        func following(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection> {
            return .init()
        }

        var following: FragmentPath<ProductHunt.UserConnection> { .init() }

        var headline: Path<String?> { .init() }

        var id: Path<String> { .init() }

        var isFollowing: Path<Bool> { .init() }

        var isMaker: Path<Bool> { .init() }

        var isViewer: Path<Bool> { .init() }

        func madePosts(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
            return .init()
        }

        var madePosts: FragmentPath<ProductHunt.PostConnection> { .init() }

        var name: Path<String> { .init() }

        func profileImage(size _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var profileImage: Path<String?> { .init() }

        func submittedPosts(first _: GraphQLArgument<Int?> = .argument,
                            after _: GraphQLArgument<String?> = .argument,
                            last _: GraphQLArgument<Int?> = .argument,
                            before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
            return .init()
        }

        var submittedPosts: FragmentPath<ProductHunt.PostConnection> { .init() }

        var twitterUsername: Path<String?> { .init() }

        var url: Path<String> { .init() }

        var username: Path<String> { .init() }

        func votedPosts(first _: GraphQLArgument<Int?> = .argument,
                        after _: GraphQLArgument<String?> = .argument,
                        last _: GraphQLArgument<Int?> = .argument,
                        before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection> {
            return .init()
        }

        var votedPosts: FragmentPath<ProductHunt.PostConnection> { .init() }

        var websiteUrl: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.User? {
        func coverImage(width _: GraphQLArgument<Int?> = .argument,
                        height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var coverImage: Path<String?> { .init() }

        var createdAt: Path<String?> { .init() }

        func followedCollections(first _: GraphQLArgument<Int?> = .argument,
                                 after _: GraphQLArgument<String?> = .argument,
                                 last _: GraphQLArgument<Int?> = .argument,
                                 before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.CollectionConnection?> {
            return .init()
        }

        var followedCollections: FragmentPath<ProductHunt.CollectionConnection?> { .init() }

        func followers(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection?> {
            return .init()
        }

        var followers: FragmentPath<ProductHunt.UserConnection?> { .init() }

        func following(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.UserConnection?> {
            return .init()
        }

        var following: FragmentPath<ProductHunt.UserConnection?> { .init() }

        var headline: Path<String?> { .init() }

        var id: Path<String?> { .init() }

        var isFollowing: Path<Bool?> { .init() }

        var isMaker: Path<Bool?> { .init() }

        var isViewer: Path<Bool?> { .init() }

        func madePosts(first _: GraphQLArgument<Int?> = .argument,
                       after _: GraphQLArgument<String?> = .argument,
                       last _: GraphQLArgument<Int?> = .argument,
                       before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection?> {
            return .init()
        }

        var madePosts: FragmentPath<ProductHunt.PostConnection?> { .init() }

        var name: Path<String?> { .init() }

        func profileImage(size _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var profileImage: Path<String?> { .init() }

        func submittedPosts(first _: GraphQLArgument<Int?> = .argument,
                            after _: GraphQLArgument<String?> = .argument,
                            last _: GraphQLArgument<Int?> = .argument,
                            before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection?> {
            return .init()
        }

        var submittedPosts: FragmentPath<ProductHunt.PostConnection?> { .init() }

        var twitterUsername: Path<String?> { .init() }

        var url: Path<String?> { .init() }

        var username: Path<String?> { .init() }

        func votedPosts(first _: GraphQLArgument<Int?> = .argument,
                        after _: GraphQLArgument<String?> = .argument,
                        last _: GraphQLArgument<Int?> = .argument,
                        before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.PostConnection?> {
            return .init()
        }

        var votedPosts: FragmentPath<ProductHunt.PostConnection?> { .init() }

        var websiteUrl: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionConnection {
        var edges: FragmentPath<[ProductHunt.CollectionEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionConnection? {
        var edges: FragmentPath<[ProductHunt.CollectionEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Collection> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Collection?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserConnection {
        var edges: FragmentPath<[ProductHunt.UserEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserConnection? {
        var edges: FragmentPath<[ProductHunt.UserEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.User> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.User?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentConnection {
        var edges: FragmentPath<[ProductHunt.CommentEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentConnection? {
        var edges: FragmentPath<[ProductHunt.CommentEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Comment> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CommentEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Comment?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Comment {
        var body: Path<String> { .init() }

        var createdAt: Path<String> { .init() }

        var id: Path<String> { .init() }

        var isVoted: Path<Bool> { .init() }

        var parent: FragmentPath<ProductHunt.Comment?> { .init() }

        var parentId: Path<String?> { .init() }

        func replies(first _: GraphQLArgument<Int?> = .argument,
                     after _: GraphQLArgument<String?> = .argument,
                     last _: GraphQLArgument<Int?> = .argument,
                     before _: GraphQLArgument<String?> = .argument,
                     order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection> {
            return .init()
        }

        var replies: FragmentPath<ProductHunt.CommentConnection> { .init() }

        var url: Path<String> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }

        var userId: Path<String> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection> { .init() }

        var votesCount: Path<Int> { .init() }

        var votableInterface: FragmentPath<ProductHunt.VotableInterface> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Comment? {
        var body: Path<String?> { .init() }

        var createdAt: Path<String?> { .init() }

        var id: Path<String?> { .init() }

        var isVoted: Path<Bool?> { .init() }

        var parent: FragmentPath<ProductHunt.Comment?> { .init() }

        var parentId: Path<String?> { .init() }

        func replies(first _: GraphQLArgument<Int?> = .argument,
                     after _: GraphQLArgument<String?> = .argument,
                     last _: GraphQLArgument<Int?> = .argument,
                     before _: GraphQLArgument<String?> = .argument,
                     order _: GraphQLArgument<ProductHunt.CommentsOrder?> = .argument) -> FragmentPath<ProductHunt.CommentConnection?> {
            return .init()
        }

        var replies: FragmentPath<ProductHunt.CommentConnection?> { .init() }

        var url: Path<String?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }

        var userId: Path<String?> { .init() }

        func votes(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   createdAfter _: GraphQLArgument<String?> = .argument,
                   createdBefore _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.VoteConnection?> {
            return .init()
        }

        var votes: FragmentPath<ProductHunt.VoteConnection?> { .init() }

        var votesCount: Path<Int?> { .init() }

        var votableInterface: FragmentPath<ProductHunt.VotableInterface?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Media {
        var type: Path<String> { .init() }

        func url(width _: GraphQLArgument<Int?> = .argument,
                 height _: GraphQLArgument<Int?> = .argument) -> Path<String> {
            return .init()
        }

        var url: Path<String> { .init() }

        var videoUrl: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Media? {
        var type: Path<String?> { .init() }

        func url(width _: GraphQLArgument<Int?> = .argument,
                 height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var url: Path<String?> { .init() }

        var videoUrl: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.ProductLink {
        var type: Path<String> { .init() }

        var url: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.ProductLink? {
        var type: Path<String?> { .init() }

        var url: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.CollectionsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Goal {
        var cheerCount: Path<Int> { .init() }

        var completedAt: Path<String?> { .init() }

        var createdAt: Path<String> { .init() }

        var current: Path<Bool> { .init() }

        var currentUntil: Path<String?> { .init() }

        var dueAt: Path<String?> { .init() }

        var focusedDuration: Path<Int> { .init() }

        var group: FragmentPath<ProductHunt.MakerGroup> { .init() }

        var groupId: Path<String> { .init() }

        var id: Path<String> { .init() }

        var isCheered: Path<Bool> { .init() }

        var project: FragmentPath<ProductHunt.MakerProject?> { .init() }

        var title: Path<String> { .init() }

        var url: Path<String> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }

        var userId: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Goal? {
        var cheerCount: Path<Int?> { .init() }

        var completedAt: Path<String?> { .init() }

        var createdAt: Path<String?> { .init() }

        var current: Path<Bool?> { .init() }

        var currentUntil: Path<String?> { .init() }

        var dueAt: Path<String?> { .init() }

        var focusedDuration: Path<Int?> { .init() }

        var group: FragmentPath<ProductHunt.MakerGroup?> { .init() }

        var groupId: Path<String?> { .init() }

        var id: Path<String?> { .init() }

        var isCheered: Path<Bool?> { .init() }

        var project: FragmentPath<ProductHunt.MakerProject?> { .init() }

        var title: Path<String?> { .init() }

        var url: Path<String?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }

        var userId: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroup {
        var description: Path<String> { .init() }

        var goalsCount: Path<Int> { .init() }

        var id: Path<String> { .init() }

        var isMember: Path<Bool> { .init() }

        var membersCount: Path<Int> { .init() }

        var name: Path<String> { .init() }

        var tagline: Path<String> { .init() }

        var url: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroup? {
        var description: Path<String?> { .init() }

        var goalsCount: Path<Int?> { .init() }

        var id: Path<String?> { .init() }

        var isMember: Path<Bool?> { .init() }

        var membersCount: Path<Int?> { .init() }

        var name: Path<String?> { .init() }

        var tagline: Path<String?> { .init() }

        var url: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProject {
        var id: Path<String> { .init() }

        func image(width _: GraphQLArgument<Int?> = .argument,
                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var image: Path<String?> { .init() }

        var lookingForOtherMakers: Path<Bool> { .init() }

        var name: Path<String> { .init() }

        var tagline: Path<String> { .init() }

        var url: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProject? {
        var id: Path<String?> { .init() }

        func image(width _: GraphQLArgument<Int?> = .argument,
                   height _: GraphQLArgument<Int?> = .argument) -> Path<String?> {
            return .init()
        }

        var image: Path<String?> { .init() }

        var lookingForOtherMakers: Path<Bool?> { .init() }

        var name: Path<String?> { .init() }

        var tagline: Path<String?> { .init() }

        var url: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalConnection {
        var edges: FragmentPath<[ProductHunt.GoalEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalConnection? {
        var edges: FragmentPath<[ProductHunt.GoalEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.Goal> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupConnection {
        var edges: FragmentPath<[ProductHunt.MakerGroupEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupConnection? {
        var edges: FragmentPath<[ProductHunt.MakerGroupEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.MakerGroup> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerGroupEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.MakerGroup?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.PostsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicsOrder {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.TopicsOrder? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Viewer {
        func goals(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   current _: GraphQLArgument<Bool?> = .argument,
                   order _: GraphQLArgument<ProductHunt.GoalsOrder?> = .argument) -> FragmentPath<ProductHunt.GoalConnection> {
            return .init()
        }

        var goals: FragmentPath<ProductHunt.GoalConnection> { .init() }

        func makerGroups(first _: GraphQLArgument<Int?> = .argument,
                         after _: GraphQLArgument<String?> = .argument,
                         last _: GraphQLArgument<Int?> = .argument,
                         before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerGroupConnection> {
            return .init()
        }

        var makerGroups: FragmentPath<ProductHunt.MakerGroupConnection> { .init() }

        func makerProjects(first _: GraphQLArgument<Int?> = .argument,
                           after _: GraphQLArgument<String?> = .argument,
                           last _: GraphQLArgument<Int?> = .argument,
                           before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerProjectConnection> {
            return .init()
        }

        var makerProjects: FragmentPath<ProductHunt.MakerProjectConnection> { .init() }

        var user: FragmentPath<ProductHunt.User> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Viewer? {
        func goals(first _: GraphQLArgument<Int?> = .argument,
                   after _: GraphQLArgument<String?> = .argument,
                   last _: GraphQLArgument<Int?> = .argument,
                   before _: GraphQLArgument<String?> = .argument,
                   current _: GraphQLArgument<Bool?> = .argument,
                   order _: GraphQLArgument<ProductHunt.GoalsOrder?> = .argument) -> FragmentPath<ProductHunt.GoalConnection?> {
            return .init()
        }

        var goals: FragmentPath<ProductHunt.GoalConnection?> { .init() }

        func makerGroups(first _: GraphQLArgument<Int?> = .argument,
                         after _: GraphQLArgument<String?> = .argument,
                         last _: GraphQLArgument<Int?> = .argument,
                         before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerGroupConnection?> {
            return .init()
        }

        var makerGroups: FragmentPath<ProductHunt.MakerGroupConnection?> { .init() }

        func makerProjects(first _: GraphQLArgument<Int?> = .argument,
                           after _: GraphQLArgument<String?> = .argument,
                           last _: GraphQLArgument<Int?> = .argument,
                           before _: GraphQLArgument<String?> = .argument) -> FragmentPath<ProductHunt.MakerProjectConnection?> {
            return .init()
        }

        var makerProjects: FragmentPath<ProductHunt.MakerProjectConnection?> { .init() }

        var user: FragmentPath<ProductHunt.User?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProjectConnection {
        var edges: FragmentPath<[ProductHunt.MakerProjectEdge]> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo> { .init() }

        var totalCount: Path<Int> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProjectConnection? {
        var edges: FragmentPath<[ProductHunt.MakerProjectEdge]?> { .init() }

        var pageInfo: FragmentPath<ProductHunt.PageInfo?> { .init() }

        var totalCount: Path<Int?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProjectEdge {
        var cursor: Path<String> { .init() }

        var node: FragmentPath<ProductHunt.MakerProject> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.MakerProjectEdge? {
        var cursor: Path<String?> { .init() }

        var node: FragmentPath<ProductHunt.MakerProject?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerPayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerPayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Error {
        var field: Path<String> { .init() }

        var message: Path<String> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.Error? {
        var field: Path<String?> { .init() }

        var message: Path<String?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerUndoInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerUndoInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerUndoPayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCheerUndoPayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCreateInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCreateInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCreatePayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalCreatePayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsCompleteInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsCompleteInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsCompletePayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsCompletePayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsIncompleteInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsIncompleteInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsIncompletePayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalMarkAsIncompletePayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalUpdateInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalUpdateInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalUpdatePayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.GoalUpdatePayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.Goal?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowPayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.User?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowPayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.User?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowUndoInput {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowUndoInput? {}

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowUndoPayload {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]> { .init() }

        var node: FragmentPath<ProductHunt.User?> { .init() }
    }

    extension GraphQLFragmentPath where UnderlyingType == ProductHunt.UserFollowUndoPayload? {
        var clientMutationId: Path<String?> { .init() }

        var errors: FragmentPath<[ProductHunt.Error]?> { .init() }

        var node: FragmentPath<ProductHunt.User?> { .init() }
    }

#endif




// MARK: - ThumbnailView

#if GRAPHAELLO_PRODUCT_HUNT_PLAYGROUND_TARGET

    extension ApolloProductHunt.ThumbnailViewPost: Fragment {
        typealias UnderlyingType = ProductHunt.Post
    }

    extension ThumbnailView {
        typealias Post = ApolloProductHunt.ThumbnailViewPost

        init(post: Post) {
            self.init(url: GraphQL(post.thumbnail?.url))
        }

        @ViewBuilder
        static func placeholderView() -> some View {
            if #available(iOS 14.0, macOS 11.0, tvOS 14.0, watchOS 7.0, *) {
                Self(post: .placeholder).disabled(true).redacted(reason: .placeholder)
            } else {
                BasicLoadingView()
            }
        }
    }

    extension ThumbnailView: Fragment {
        typealias UnderlyingType = ProductHunt.Post

        static let placeholder = Self(post: .placeholder)
    }

    extension ApolloProductHunt.ThumbnailViewPost {
        func referencedSingleFragmentStruct() -> ThumbnailView {
            return ThumbnailView(post: self)
        }
    }

    extension ApolloProductHunt.ThumbnailViewPost {
        private static let placeholderMap: ResultMap = ["__typename": "Post", "thumbnail": ["__typename": "Media", "url": "__GRAPHAELLO_PLACEHOLDER__"]]

        static let placeholder = ApolloProductHunt.ThumbnailViewPost(
            unsafeResultMap: ApolloProductHunt.ThumbnailViewPost.placeholderMap
        )
    }

#endif


// MARK: - PostCell

#if GRAPHAELLO_PRODUCT_HUNT_PLAYGROUND_TARGET

    extension ApolloProductHunt.PostCellPost: Fragment {
        typealias UnderlyingType = ProductHunt.Post
    }

    extension PostCell {
        typealias Post = ApolloProductHunt.PostCellPost

        init(post: Post) {
            self.init(name: GraphQL(post.name),
                      tagline: GraphQL(post.tagline),
                      votesCount: GraphQL(post.votesCount),
                      post: GraphQL(post.fragments.thumbnailViewPost))
        }

        @ViewBuilder
        static func placeholderView() -> some View {
            if #available(iOS 14.0, macOS 11.0, tvOS 14.0, watchOS 7.0, *) {
                Self(post: .placeholder).disabled(true).redacted(reason: .placeholder)
            } else {
                BasicLoadingView()
            }
        }
    }

    extension PostCell: Fragment {
        typealias UnderlyingType = ProductHunt.Post

        static let placeholder = Self(post: .placeholder)
    }

    extension ApolloProductHunt.PostCellPost {
        func referencedSingleFragmentStruct() -> PostCell {
            return PostCell(post: self)
        }
    }

    extension ApolloProductHunt.PostCellPost {
        private static let placeholderMap: ResultMap = ["__typename": "Post", "name": "__GRAPHAELLO_PLACEHOLDER__", "tagline": "__GRAPHAELLO_PLACEHOLDER__", "thumbnail": ["__typename": "Media", "url": "__GRAPHAELLO_PLACEHOLDER__"], "votesCount": 42]

        static let placeholder = ApolloProductHunt.PostCellPost(
            unsafeResultMap: ApolloProductHunt.PostCellPost.placeholderMap
        )
    }

    extension ApolloProductHunt.PostCellPost {
        public struct Fragments {
            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
                resultMap = unsafeResultMap
            }
        }

        public var fragments: Fragments {
            get {
                return Fragments(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

    extension ApolloProductHunt.PostCellPost.Fragments {
        public var thumbnailViewPost: ApolloProductHunt.ThumbnailViewPost {
            get {
                return ApolloProductHunt.ThumbnailViewPost(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

#endif


// MARK: - ContentView

#if GRAPHAELLO_PRODUCT_HUNT_PLAYGROUND_TARGET

    extension ContentView {
        typealias Data = ApolloProductHunt.ContentViewQuery.Data

        init(posts: Paging<PostCell.Post>,
             data _: Data) {
            self.init(posts: GraphQL(posts))
        }

        @ViewBuilder
        static func placeholderView(posts: Paging<PostCell.Post>) -> some View {
            if #available(iOS 14.0, macOS 11.0, tvOS 14.0, watchOS 7.0, *) {
                Self(posts: posts,
                     data: .placeholder).disabled(true).redacted(reason: .placeholder)
            } else {
                BasicLoadingView()
            }
        }
    }

    extension ProductHunt {
        func contentView<Loading: View, Error: View>(first: Int? = nil,
                                                     after: String? = nil,
                                                     last: Int? = nil,
                                                     before: String? = nil,
                                                     featured: Bool? = nil,
                                                     postedBefore: String? = nil,
                                                     postedAfter: String? = nil,
                                                     topic: String? = nil,
                                                     order: ProductHunt.PostsOrder? = ProductHunt.PostsOrder.ranking,
                                                     twitterUrl: String? = nil,
                                                     url: String? = nil,
                                                     width: Int? = nil,
                                                     height: Int? = nil,
                                                     
                                                     @ViewBuilder loading: () -> Loading,
                                                     @ViewBuilder error: @escaping (QueryError) -> Error) -> some View {
            return QueryRenderer(client: client,
                                 query: ApolloProductHunt.ContentViewQuery(first: first,
                                                                           after: after,
                                                                           last: last,
                                                                           before: before,
                                                                           featured: featured,
                                                                           postedBefore: postedBefore,
                                                                           postedAfter: postedAfter,
                                                                           topic: topic,
                                                                           order: order,
                                                                           twitterUrl: twitterUrl,
                                                                           url: url,
                                                                           width: width,
                                                                           height: height),
                                 loading: loading(),
                                 error: error) { (data: ApolloProductHunt.ContentViewQuery.Data) -> ContentView in

                ContentView(posts: data.posts.fragments.postConnectionPostCellPost.paging { _cursor, _pageSize, _completion in
                    self.client.fetch(query: ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery(first: _pageSize ?? first,
                                                                                                               after: _cursor,
                                                                                                               last: last,
                                                                                                               before: before,
                                                                                                               featured: featured,
                                                                                                               postedBefore: postedBefore,
                                                                                                               postedAfter: postedAfter,
                                                                                                               topic: topic,
                                                                                                               order: order,
                                                                                                               twitterUrl: twitterUrl,
                                                                                                               url: url,
                                                                                                               width: width,
                                                                                                               height: height)) { result in
                        _completion(result.map { $0.data?.posts.fragments.postConnectionPostCellPost })
                    }
                },
                            
                data: data)
            }
        }

        func contentView<Loading: View>(first: Int? = nil,
                                        after: String? = nil,
                                        last: Int? = nil,
                                        before: String? = nil,
                                        featured: Bool? = nil,
                                        postedBefore: String? = nil,
                                        postedAfter: String? = nil,
                                        topic: String? = nil,
                                        order: ProductHunt.PostsOrder? = ProductHunt.PostsOrder.ranking,
                                        twitterUrl: String? = nil,
                                        url: String? = nil,
                                        width: Int? = nil,
                                        height: Int? = nil,
                                        
                                        @ViewBuilder loading: () -> Loading) -> some View {
            return QueryRenderer(client: client,
                                 query: ApolloProductHunt.ContentViewQuery(first: first,
                                                                           after: after,
                                                                           last: last,
                                                                           before: before,
                                                                           featured: featured,
                                                                           postedBefore: postedBefore,
                                                                           postedAfter: postedAfter,
                                                                           topic: topic,
                                                                           order: order,
                                                                           twitterUrl: twitterUrl,
                                                                           url: url,
                                                                           width: width,
                                                                           height: height),
                                 loading: loading(),
                                 error: { BasicErrorView(error: $0) }) { (data: ApolloProductHunt.ContentViewQuery.Data) -> ContentView in

                ContentView(posts: data.posts.fragments.postConnectionPostCellPost.paging { _cursor, _pageSize, _completion in
                    self.client.fetch(query: ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery(first: _pageSize ?? first,
                                                                                                               after: _cursor,
                                                                                                               last: last,
                                                                                                               before: before,
                                                                                                               featured: featured,
                                                                                                               postedBefore: postedBefore,
                                                                                                               postedAfter: postedAfter,
                                                                                                               topic: topic,
                                                                                                               order: order,
                                                                                                               twitterUrl: twitterUrl,
                                                                                                               url: url,
                                                                                                               width: width,
                                                                                                               height: height)) { result in
                        _completion(result.map { $0.data?.posts.fragments.postConnectionPostCellPost })
                    }
                },
                            
                data: data)
            }
        }

        func contentView<Error: View>(first: Int? = nil,
                                      after: String? = nil,
                                      last: Int? = nil,
                                      before: String? = nil,
                                      featured: Bool? = nil,
                                      postedBefore: String? = nil,
                                      postedAfter: String? = nil,
                                      topic: String? = nil,
                                      order: ProductHunt.PostsOrder? = ProductHunt.PostsOrder.ranking,
                                      twitterUrl: String? = nil,
                                      url: String? = nil,
                                      width: Int? = nil,
                                      height: Int? = nil,
                                      
                                      @ViewBuilder error: @escaping (QueryError) -> Error) -> some View {
            return QueryRenderer(client: client,
                                 query: ApolloProductHunt.ContentViewQuery(first: first,
                                                                           after: after,
                                                                           last: last,
                                                                           before: before,
                                                                           featured: featured,
                                                                           postedBefore: postedBefore,
                                                                           postedAfter: postedAfter,
                                                                           topic: topic,
                                                                           order: order,
                                                                           twitterUrl: twitterUrl,
                                                                           url: url,
                                                                           width: width,
                                                                           height: height),
                                 loading: ContentView.placeholderView(posts: ApolloProductHunt.ContentViewQuery.Data.placeholder.posts.fragments.postConnectionPostCellPost.paging { _, _, _ in
                                     // no-op
                                 }),
                                 error: error) { (data: ApolloProductHunt.ContentViewQuery.Data) -> ContentView in

                ContentView(posts: data.posts.fragments.postConnectionPostCellPost.paging { _cursor, _pageSize, _completion in
                    self.client.fetch(query: ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery(first: _pageSize ?? first,
                                                                                                               after: _cursor,
                                                                                                               last: last,
                                                                                                               before: before,
                                                                                                               featured: featured,
                                                                                                               postedBefore: postedBefore,
                                                                                                               postedAfter: postedAfter,
                                                                                                               topic: topic,
                                                                                                               order: order,
                                                                                                               twitterUrl: twitterUrl,
                                                                                                               url: url,
                                                                                                               width: width,
                                                                                                               height: height)) { result in
                        _completion(result.map { $0.data?.posts.fragments.postConnectionPostCellPost })
                    }
                },
                            
                data: data)
            }
        }

        func contentView(first: Int? = nil,
                         after: String? = nil,
                         last: Int? = nil,
                         before: String? = nil,
                         featured: Bool? = nil,
                         postedBefore: String? = nil,
                         postedAfter: String? = nil,
                         topic: String? = nil,
                         order: ProductHunt.PostsOrder? = ProductHunt.PostsOrder.ranking,
                         twitterUrl: String? = nil,
                         url: String? = nil,
                         width: Int? = nil,
                         height: Int? = nil) -> some View {
            return QueryRenderer(client: client,
                                 query: ApolloProductHunt.ContentViewQuery(first: first,
                                                                           after: after,
                                                                           last: last,
                                                                           before: before,
                                                                           featured: featured,
                                                                           postedBefore: postedBefore,
                                                                           postedAfter: postedAfter,
                                                                           topic: topic,
                                                                           order: order,
                                                                           twitterUrl: twitterUrl,
                                                                           url: url,
                                                                           width: width,
                                                                           height: height),
                                 loading: ContentView.placeholderView(posts: ApolloProductHunt.ContentViewQuery.Data.placeholder.posts.fragments.postConnectionPostCellPost.paging { _, _, _ in
                                     // no-op
                                 }),
                                 error: { BasicErrorView(error: $0) }) { (data: ApolloProductHunt.ContentViewQuery.Data) -> ContentView in

                ContentView(posts: data.posts.fragments.postConnectionPostCellPost.paging { _cursor, _pageSize, _completion in
                    self.client.fetch(query: ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery(first: _pageSize ?? first,
                                                                                                               after: _cursor,
                                                                                                               last: last,
                                                                                                               before: before,
                                                                                                               featured: featured,
                                                                                                               postedBefore: postedBefore,
                                                                                                               postedAfter: postedAfter,
                                                                                                               topic: topic,
                                                                                                               order: order,
                                                                                                               twitterUrl: twitterUrl,
                                                                                                               url: url,
                                                                                                               width: width,
                                                                                                               height: height)) { result in
                        _completion(result.map { $0.data?.posts.fragments.postConnectionPostCellPost })
                    }
                },
                            
                data: data)
            }
        }
    }

    extension ApolloProductHunt.ContentViewQuery.Data {
        private static let placeholderMap: ResultMap = ["posts": ["__typename": "PostConnection", "edges": Array(repeating: ["__typename": "PostEdge", "node": ["__typename": "Post", "name": "__GRAPHAELLO_PLACEHOLDER__", "tagline": "__GRAPHAELLO_PLACEHOLDER__", "thumbnail": ["__typename": "Media", "url": "__GRAPHAELLO_PLACEHOLDER__"], "votesCount": 42]], count: 5) as [ResultMap], "pageInfo": ["__typename": "PageInfo", "endCursor": "__GRAPHAELLO_PLACEHOLDER__", "hasNextPage": true]]]

        static let placeholder = ApolloProductHunt.ContentViewQuery.Data(
            unsafeResultMap: ApolloProductHunt.ContentViewQuery.Data.placeholderMap
        )
    }

    extension ApolloProductHunt.ContentViewQuery.Data.Post {
        public struct Fragments {
            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
                resultMap = unsafeResultMap
            }
        }

        public var fragments: Fragments {
            get {
                return Fragments(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

    extension ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery.Data.Post {
        public struct Fragments {
            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
                resultMap = unsafeResultMap
            }
        }

        public var fragments: Fragments {
            get {
                return Fragments(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

    extension ApolloProductHunt.ContentViewQuery.Data.Post.Fragments {
        public var postConnectionPostCellPost: ApolloProductHunt.PostConnectionPostCellPost {
            get {
                return ApolloProductHunt.PostConnectionPostCellPost(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

    extension ApolloProductHunt.ContentViewPostsPostConnectionPostCellPostQuery.Data.Post.Fragments {
        public var postConnectionPostCellPost: ApolloProductHunt.PostConnectionPostCellPost {
            get {
                return ApolloProductHunt.PostConnectionPostCellPost(unsafeResultMap: resultMap)
            }
            set {
                resultMap += newValue.resultMap
            }
        }
    }

#endif




extension ApolloProductHunt.PostConnectionPostCellPost {
    typealias Completion = (Result<ApolloProductHunt.PostConnectionPostCellPost?, Error>) -> Void
    typealias Loader = (String, Int?, @escaping Completion) -> Void

    private var response: Paging<ApolloProductHunt.PostCellPost>.Response {
        return Paging.Response(values: edges.compactMap { $0.node.fragments.postCellPost },
                               cursor: pageInfo.endCursor,
                               hasMore: pageInfo.hasNextPage)
    }

    fileprivate func paging(loader: @escaping Loader) -> Paging<ApolloProductHunt.PostCellPost> {
        return Paging(response) { cursor, pageSize, completion in
            loader(cursor, pageSize) { result in
                completion(result.map { $0?.response ?? .empty })
            }
        }
    }
}

extension ApolloProductHunt.PostConnectionPostCellPost.Edge.Node {
    public struct Fragments {
        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
            resultMap = unsafeResultMap
        }
    }

    public var fragments: Fragments {
        get {
            return Fragments(unsafeResultMap: resultMap)
        }
        set {
            resultMap += newValue.resultMap
        }
    }
}

extension ApolloProductHunt.PostConnectionPostCellPost.Edge.Node.Fragments {
    public var postCellPost: ApolloProductHunt.PostCellPost {
        get {
            return ApolloProductHunt.PostCellPost(unsafeResultMap: resultMap)
        }
        set {
            resultMap += newValue.resultMap
        }
    }
}

extension ApolloProductHunt.PostConnectionPostCellPost.Edge.Node.Fragments {
    public var thumbnailViewPost: ApolloProductHunt.ThumbnailViewPost {
        get {
            return ApolloProductHunt.ThumbnailViewPost(unsafeResultMap: resultMap)
        }
        set {
            resultMap += newValue.resultMap
        }
    }
}




// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

/// ApolloProductHunt namespace
public enum ApolloProductHunt {
  public enum PostsOrder: RawRepresentable, Equatable, Hashable, CaseIterable, Apollo.JSONDecodable, Apollo.JSONEncodable {
    public typealias RawValue = String
    /// Returns Posts in descending order of featured date.
    case featuredAt
    /// Returns Posts in descending order of votes count.
    case votes
    /// Returns Posts in descending order of ranking.
    case ranking
    /// Returns Posts in descending order of creation date.
    case newest
    /// Auto generated constant for unknown enum values
    case __unknown(RawValue)

    public init?(rawValue: RawValue) {
      switch rawValue {
        case "FEATURED_AT": self = .featuredAt
        case "VOTES": self = .votes
        case "RANKING": self = .ranking
        case "NEWEST": self = .newest
        default: self = .__unknown(rawValue)
      }
    }

    public var rawValue: RawValue {
      switch self {
        case .featuredAt: return "FEATURED_AT"
        case .votes: return "VOTES"
        case .ranking: return "RANKING"
        case .newest: return "NEWEST"
        case .__unknown(let value): return value
      }
    }

    public static func == (lhs: PostsOrder, rhs: PostsOrder) -> Bool {
      switch (lhs, rhs) {
        case (.featuredAt, .featuredAt): return true
        case (.votes, .votes): return true
        case (.ranking, .ranking): return true
        case (.newest, .newest): return true
        case (.__unknown(let lhsValue), .__unknown(let rhsValue)): return lhsValue == rhsValue
        default: return false
      }
    }

    public static var allCases: [PostsOrder] {
      return [
        .featuredAt,
        .votes,
        .ranking,
        .newest,
      ]
    }
  }

  public final class ContentViewPostsPostConnectionPostCellPostQuery: GraphQLQuery {
    /// The raw GraphQL definition of this operation.
    public let operationDefinition: String =
      """
      query ContentViewPostsPostConnectionPostCellPost($first: Int, $after: String, $last: Int, $before: String, $featured: Boolean, $postedBefore: DateTime, $postedAfter: DateTime, $topic: String, $order: PostsOrder, $twitterUrl: String, $url: String, $width: Int, $height: Int) {
        posts(
          after: $after
          before: $before
          featured: $featured
          first: $first
          last: $last
          order: $order
          postedAfter: $postedAfter
          postedBefore: $postedBefore
          topic: $topic
          twitterUrl: $twitterUrl
          url: $url
        ) {
          __typename
          edges {
            __typename
            node {
              __typename
              thumbnail {
                __typename
                url(height: $height, width: $width)
              }
              name
              tagline
              votesCount
              thumbnail {
                __typename
                url(height: $height, width: $width)
              }
            }
          }
          pageInfo {
            __typename
            endCursor
            hasNextPage
          }
        }
      }
      """

    public let operationName: String = "ContentViewPostsPostConnectionPostCellPost"

    public var first: Int?
    public var after: String?
    public var last: Int?
    public var before: String?
    public var featured: Bool?
    public var postedBefore: String?
    public var postedAfter: String?
    public var topic: String?
    public var order: PostsOrder?
    public var twitterUrl: String?
    public var url: String?
    public var width: Int?
    public var height: Int?

    public init(first: Int? = nil, after: String? = nil, last: Int? = nil, before: String? = nil, featured: Bool? = nil, postedBefore: String? = nil, postedAfter: String? = nil, topic: String? = nil, order: PostsOrder? = nil, twitterUrl: String? = nil, url: String? = nil, width: Int? = nil, height: Int? = nil) {
      self.first = first
      self.after = after
      self.last = last
      self.before = before
      self.featured = featured
      self.postedBefore = postedBefore
      self.postedAfter = postedAfter
      self.topic = topic
      self.order = order
      self.twitterUrl = twitterUrl
      self.url = url
      self.width = width
      self.height = height
    }

    public var variables: GraphQLMap? {
      return ["first": first, "after": after, "last": last, "before": before, "featured": featured, "postedBefore": postedBefore, "postedAfter": postedAfter, "topic": topic, "order": order, "twitterUrl": twitterUrl, "url": url, "width": width, "height": height]
    }

    public struct Data: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Query"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("posts", arguments: ["after": GraphQLVariable("after"), "before": GraphQLVariable("before"), "featured": GraphQLVariable("featured"), "first": GraphQLVariable("first"), "last": GraphQLVariable("last"), "order": GraphQLVariable("order"), "postedAfter": GraphQLVariable("postedAfter"), "postedBefore": GraphQLVariable("postedBefore"), "topic": GraphQLVariable("topic"), "twitterUrl": GraphQLVariable("twitterUrl"), "url": GraphQLVariable("url")], type: .nonNull(.object(Post.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(posts: Post) {
        self.init(unsafeResultMap: ["__typename": "Query", "posts": posts.resultMap])
      }

      /// Look up Posts by various parameters.
      public var posts: Post {
        get {
          return Post(unsafeResultMap: resultMap["posts"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "posts")
        }
      }

      public struct Post: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PostConnection"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("edges", type: .nonNull(.list(.nonNull(.object(Edge.selections))))),
            GraphQLField("pageInfo", type: .nonNull(.object(PageInfo.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(edges: [Edge], pageInfo: PageInfo) {
          self.init(unsafeResultMap: ["__typename": "PostConnection", "edges": edges.map { (value: Edge) -> ResultMap in value.resultMap }, "pageInfo": pageInfo.resultMap])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// A list of edges.
        public var edges: [Edge] {
          get {
            return (resultMap["edges"] as! [ResultMap]).map { (value: ResultMap) -> Edge in Edge(unsafeResultMap: value) }
          }
          set {
            resultMap.updateValue(newValue.map { (value: Edge) -> ResultMap in value.resultMap }, forKey: "edges")
          }
        }

        /// Information to aid in pagination.
        public var pageInfo: PageInfo {
          get {
            return PageInfo(unsafeResultMap: resultMap["pageInfo"]! as! ResultMap)
          }
          set {
            resultMap.updateValue(newValue.resultMap, forKey: "pageInfo")
          }
        }

        public struct Edge: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PostEdge"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("node", type: .nonNull(.object(Node.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(node: Node) {
            self.init(unsafeResultMap: ["__typename": "PostEdge", "node": node.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// The item at the end of the edge.
          public var node: Node {
            get {
              return Node(unsafeResultMap: resultMap["node"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "node")
            }
          }

          public struct Node: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Post"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
                GraphQLField("name", type: .nonNull(.scalar(String.self))),
                GraphQLField("tagline", type: .nonNull(.scalar(String.self))),
                GraphQLField("votesCount", type: .nonNull(.scalar(Int.self))),
                GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(thumbnail: Thumbnail? = nil, name: String, tagline: String, votesCount: Int) {
              self.init(unsafeResultMap: ["__typename": "Post", "thumbnail": thumbnail.flatMap { (value: Thumbnail) -> ResultMap in value.resultMap }, "name": name, "tagline": tagline, "votesCount": votesCount])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Thumbnail media object of the Post.
            public var thumbnail: Thumbnail? {
              get {
                return (resultMap["thumbnail"] as? ResultMap).flatMap { Thumbnail(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "thumbnail")
              }
            }

            /// Name of the Post.
            public var name: String {
              get {
                return resultMap["name"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            /// Tagline of the Post.
            public var tagline: String {
              get {
                return resultMap["tagline"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "tagline")
              }
            }

            /// Number of votes that the object has currently.
            public var votesCount: Int {
              get {
                return resultMap["votesCount"]! as! Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "votesCount")
              }
            }

            public struct Thumbnail: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Media"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("url", arguments: ["height": GraphQLVariable("height"), "width": GraphQLVariable("width")], type: .nonNull(.scalar(String.self))),
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("url", arguments: ["height": GraphQLVariable("height"), "width": GraphQLVariable("width")], type: .nonNull(.scalar(String.self))),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(url: String) {
                self.init(unsafeResultMap: ["__typename": "Media", "url": url])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              /// Public URL for the media object. Incase of videos this URL represents thumbnail generated from video.
              public var url: String {
                get {
                  return resultMap["url"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "url")
                }
              }
            }
          }
        }

        public struct PageInfo: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PageInfo"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("endCursor", type: .scalar(String.self)),
              GraphQLField("hasNextPage", type: .nonNull(.scalar(Bool.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(endCursor: String? = nil, hasNextPage: Bool) {
            self.init(unsafeResultMap: ["__typename": "PageInfo", "endCursor": endCursor, "hasNextPage": hasNextPage])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// When paginating forwards, the cursor to continue.
          public var endCursor: String? {
            get {
              return resultMap["endCursor"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "endCursor")
            }
          }

          /// When paginating forwards, are there more items?
          public var hasNextPage: Bool {
            get {
              return resultMap["hasNextPage"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "hasNextPage")
            }
          }
        }
      }
    }
  }

  public final class ContentViewQuery: GraphQLQuery {
    /// The raw GraphQL definition of this operation.
    public let operationDefinition: String =
      """
      query ContentView($first: Int, $after: String, $last: Int, $before: String, $featured: Boolean, $postedBefore: DateTime, $postedAfter: DateTime, $topic: String, $order: PostsOrder, $twitterUrl: String, $url: String, $width: Int, $height: Int) {
        posts(
          after: $after
          before: $before
          featured: $featured
          first: $first
          last: $last
          order: $order
          postedAfter: $postedAfter
          postedBefore: $postedBefore
          topic: $topic
          twitterUrl: $twitterUrl
          url: $url
        ) {
          __typename
          edges {
            __typename
            node {
              __typename
              thumbnail {
                __typename
                url(height: $height, width: $width)
              }
              name
              tagline
              votesCount
              thumbnail {
                __typename
                url(height: $height, width: $width)
              }
            }
          }
          pageInfo {
            __typename
            endCursor
            hasNextPage
          }
        }
      }
      """

    public let operationName: String = "ContentView"

    public var first: Int?
    public var after: String?
    public var last: Int?
    public var before: String?
    public var featured: Bool?
    public var postedBefore: String?
    public var postedAfter: String?
    public var topic: String?
    public var order: PostsOrder?
    public var twitterUrl: String?
    public var url: String?
    public var width: Int?
    public var height: Int?

    public init(first: Int? = nil, after: String? = nil, last: Int? = nil, before: String? = nil, featured: Bool? = nil, postedBefore: String? = nil, postedAfter: String? = nil, topic: String? = nil, order: PostsOrder? = nil, twitterUrl: String? = nil, url: String? = nil, width: Int? = nil, height: Int? = nil) {
      self.first = first
      self.after = after
      self.last = last
      self.before = before
      self.featured = featured
      self.postedBefore = postedBefore
      self.postedAfter = postedAfter
      self.topic = topic
      self.order = order
      self.twitterUrl = twitterUrl
      self.url = url
      self.width = width
      self.height = height
    }

    public var variables: GraphQLMap? {
      return ["first": first, "after": after, "last": last, "before": before, "featured": featured, "postedBefore": postedBefore, "postedAfter": postedAfter, "topic": topic, "order": order, "twitterUrl": twitterUrl, "url": url, "width": width, "height": height]
    }

    public struct Data: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Query"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("posts", arguments: ["after": GraphQLVariable("after"), "before": GraphQLVariable("before"), "featured": GraphQLVariable("featured"), "first": GraphQLVariable("first"), "last": GraphQLVariable("last"), "order": GraphQLVariable("order"), "postedAfter": GraphQLVariable("postedAfter"), "postedBefore": GraphQLVariable("postedBefore"), "topic": GraphQLVariable("topic"), "twitterUrl": GraphQLVariable("twitterUrl"), "url": GraphQLVariable("url")], type: .nonNull(.object(Post.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(posts: Post) {
        self.init(unsafeResultMap: ["__typename": "Query", "posts": posts.resultMap])
      }

      /// Look up Posts by various parameters.
      public var posts: Post {
        get {
          return Post(unsafeResultMap: resultMap["posts"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "posts")
        }
      }

      public struct Post: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["PostConnection"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("edges", type: .nonNull(.list(.nonNull(.object(Edge.selections))))),
            GraphQLField("pageInfo", type: .nonNull(.object(PageInfo.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(edges: [Edge], pageInfo: PageInfo) {
          self.init(unsafeResultMap: ["__typename": "PostConnection", "edges": edges.map { (value: Edge) -> ResultMap in value.resultMap }, "pageInfo": pageInfo.resultMap])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// A list of edges.
        public var edges: [Edge] {
          get {
            return (resultMap["edges"] as! [ResultMap]).map { (value: ResultMap) -> Edge in Edge(unsafeResultMap: value) }
          }
          set {
            resultMap.updateValue(newValue.map { (value: Edge) -> ResultMap in value.resultMap }, forKey: "edges")
          }
        }

        /// Information to aid in pagination.
        public var pageInfo: PageInfo {
          get {
            return PageInfo(unsafeResultMap: resultMap["pageInfo"]! as! ResultMap)
          }
          set {
            resultMap.updateValue(newValue.resultMap, forKey: "pageInfo")
          }
        }

        public struct Edge: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PostEdge"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("node", type: .nonNull(.object(Node.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(node: Node) {
            self.init(unsafeResultMap: ["__typename": "PostEdge", "node": node.resultMap])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// The item at the end of the edge.
          public var node: Node {
            get {
              return Node(unsafeResultMap: resultMap["node"]! as! ResultMap)
            }
            set {
              resultMap.updateValue(newValue.resultMap, forKey: "node")
            }
          }

          public struct Node: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Post"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
                GraphQLField("name", type: .nonNull(.scalar(String.self))),
                GraphQLField("tagline", type: .nonNull(.scalar(String.self))),
                GraphQLField("votesCount", type: .nonNull(.scalar(Int.self))),
                GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(thumbnail: Thumbnail? = nil, name: String, tagline: String, votesCount: Int) {
              self.init(unsafeResultMap: ["__typename": "Post", "thumbnail": thumbnail.flatMap { (value: Thumbnail) -> ResultMap in value.resultMap }, "name": name, "tagline": tagline, "votesCount": votesCount])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            /// Thumbnail media object of the Post.
            public var thumbnail: Thumbnail? {
              get {
                return (resultMap["thumbnail"] as? ResultMap).flatMap { Thumbnail(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "thumbnail")
              }
            }

            /// Name of the Post.
            public var name: String {
              get {
                return resultMap["name"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            /// Tagline of the Post.
            public var tagline: String {
              get {
                return resultMap["tagline"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "tagline")
              }
            }

            /// Number of votes that the object has currently.
            public var votesCount: Int {
              get {
                return resultMap["votesCount"]! as! Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "votesCount")
              }
            }

            public struct Thumbnail: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Media"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("url", arguments: ["height": GraphQLVariable("height"), "width": GraphQLVariable("width")], type: .nonNull(.scalar(String.self))),
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("url", arguments: ["height": GraphQLVariable("height"), "width": GraphQLVariable("width")], type: .nonNull(.scalar(String.self))),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(url: String) {
                self.init(unsafeResultMap: ["__typename": "Media", "url": url])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              /// Public URL for the media object. Incase of videos this URL represents thumbnail generated from video.
              public var url: String {
                get {
                  return resultMap["url"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "url")
                }
              }
            }
          }
        }

        public struct PageInfo: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["PageInfo"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("endCursor", type: .scalar(String.self)),
              GraphQLField("hasNextPage", type: .nonNull(.scalar(Bool.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(endCursor: String? = nil, hasNextPage: Bool) {
            self.init(unsafeResultMap: ["__typename": "PageInfo", "endCursor": endCursor, "hasNextPage": hasNextPage])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// When paginating forwards, the cursor to continue.
          public var endCursor: String? {
            get {
              return resultMap["endCursor"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "endCursor")
            }
          }

          /// When paginating forwards, are there more items?
          public var hasNextPage: Bool {
            get {
              return resultMap["hasNextPage"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "hasNextPage")
            }
          }
        }
      }
    }
  }

  public struct PostConnectionPostCellPost: GraphQLFragment {
    /// The raw GraphQL definition of this fragment.
    public static let fragmentDefinition: String =
      """
      fragment PostConnectionPostCellPost on PostConnection {
        __typename
        edges {
          __typename
          node {
            __typename
            thumbnail {
              __typename
              url
            }
            name
            tagline
            votesCount
            thumbnail {
              __typename
              url
            }
          }
        }
        pageInfo {
          __typename
          endCursor
          hasNextPage
        }
      }
      """

    public static let possibleTypes: [String] = ["PostConnection"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("edges", type: .nonNull(.list(.nonNull(.object(Edge.selections))))),
        GraphQLField("pageInfo", type: .nonNull(.object(PageInfo.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(edges: [Edge], pageInfo: PageInfo) {
      self.init(unsafeResultMap: ["__typename": "PostConnection", "edges": edges.map { (value: Edge) -> ResultMap in value.resultMap }, "pageInfo": pageInfo.resultMap])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    /// A list of edges.
    public var edges: [Edge] {
      get {
        return (resultMap["edges"] as! [ResultMap]).map { (value: ResultMap) -> Edge in Edge(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Edge) -> ResultMap in value.resultMap }, forKey: "edges")
      }
    }

    /// Information to aid in pagination.
    public var pageInfo: PageInfo {
      get {
        return PageInfo(unsafeResultMap: resultMap["pageInfo"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "pageInfo")
      }
    }

    public struct Edge: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PostEdge"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("node", type: .nonNull(.object(Node.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(node: Node) {
        self.init(unsafeResultMap: ["__typename": "PostEdge", "node": node.resultMap])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// The item at the end of the edge.
      public var node: Node {
        get {
          return Node(unsafeResultMap: resultMap["node"]! as! ResultMap)
        }
        set {
          resultMap.updateValue(newValue.resultMap, forKey: "node")
        }
      }

      public struct Node: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Post"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
            GraphQLField("tagline", type: .nonNull(.scalar(String.self))),
            GraphQLField("votesCount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(thumbnail: Thumbnail? = nil, name: String, tagline: String, votesCount: Int) {
          self.init(unsafeResultMap: ["__typename": "Post", "thumbnail": thumbnail.flatMap { (value: Thumbnail) -> ResultMap in value.resultMap }, "name": name, "tagline": tagline, "votesCount": votesCount])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// Thumbnail media object of the Post.
        public var thumbnail: Thumbnail? {
          get {
            return (resultMap["thumbnail"] as? ResultMap).flatMap { Thumbnail(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "thumbnail")
          }
        }

        /// Name of the Post.
        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        /// Tagline of the Post.
        public var tagline: String {
          get {
            return resultMap["tagline"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "tagline")
          }
        }

        /// Number of votes that the object has currently.
        public var votesCount: Int {
          get {
            return resultMap["votesCount"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "votesCount")
          }
        }

        public struct Thumbnail: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Media"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("url", type: .nonNull(.scalar(String.self))),
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("url", type: .nonNull(.scalar(String.self))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(url: String) {
            self.init(unsafeResultMap: ["__typename": "Media", "url": url])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// Public URL for the media object. Incase of videos this URL represents thumbnail generated from video.
          public var url: String {
            get {
              return resultMap["url"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "url")
            }
          }
        }
      }
    }

    public struct PageInfo: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["PageInfo"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("endCursor", type: .scalar(String.self)),
          GraphQLField("hasNextPage", type: .nonNull(.scalar(Bool.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(endCursor: String? = nil, hasNextPage: Bool) {
        self.init(unsafeResultMap: ["__typename": "PageInfo", "endCursor": endCursor, "hasNextPage": hasNextPage])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// When paginating forwards, the cursor to continue.
      public var endCursor: String? {
        get {
          return resultMap["endCursor"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endCursor")
        }
      }

      /// When paginating forwards, are there more items?
      public var hasNextPage: Bool {
        get {
          return resultMap["hasNextPage"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "hasNextPage")
        }
      }
    }
  }

  public struct ThumbnailViewPost: GraphQLFragment {
    /// The raw GraphQL definition of this fragment.
    public static let fragmentDefinition: String =
      """
      fragment ThumbnailViewPost on Post {
        __typename
        thumbnail {
          __typename
          url
        }
      }
      """

    public static let possibleTypes: [String] = ["Post"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(thumbnail: Thumbnail? = nil) {
      self.init(unsafeResultMap: ["__typename": "Post", "thumbnail": thumbnail.flatMap { (value: Thumbnail) -> ResultMap in value.resultMap }])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    /// Thumbnail media object of the Post.
    public var thumbnail: Thumbnail? {
      get {
        return (resultMap["thumbnail"] as? ResultMap).flatMap { Thumbnail(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "thumbnail")
      }
    }

    public struct Thumbnail: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Media"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("url", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(url: String) {
        self.init(unsafeResultMap: ["__typename": "Media", "url": url])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Public URL for the media object. Incase of videos this URL represents thumbnail generated from video.
      public var url: String {
        get {
          return resultMap["url"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "url")
        }
      }
    }
  }

  public struct PostCellPost: GraphQLFragment {
    /// The raw GraphQL definition of this fragment.
    public static let fragmentDefinition: String =
      """
      fragment PostCellPost on Post {
        __typename
        thumbnail {
          __typename
          url
        }
        name
        tagline
        votesCount
      }
      """

    public static let possibleTypes: [String] = ["Post"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("thumbnail", type: .object(Thumbnail.selections)),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("tagline", type: .nonNull(.scalar(String.self))),
        GraphQLField("votesCount", type: .nonNull(.scalar(Int.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(thumbnail: Thumbnail? = nil, name: String, tagline: String, votesCount: Int) {
      self.init(unsafeResultMap: ["__typename": "Post", "thumbnail": thumbnail.flatMap { (value: Thumbnail) -> ResultMap in value.resultMap }, "name": name, "tagline": tagline, "votesCount": votesCount])
    }

    public var __typename: String {
      get {
        return resultMap["__typename"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "__typename")
      }
    }

    /// Thumbnail media object of the Post.
    public var thumbnail: Thumbnail? {
      get {
        return (resultMap["thumbnail"] as? ResultMap).flatMap { Thumbnail(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "thumbnail")
      }
    }

    /// Name of the Post.
    public var name: String {
      get {
        return resultMap["name"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "name")
      }
    }

    /// Tagline of the Post.
    public var tagline: String {
      get {
        return resultMap["tagline"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "tagline")
      }
    }

    /// Number of votes that the object has currently.
    public var votesCount: Int {
      get {
        return resultMap["votesCount"]! as! Int
      }
      set {
        resultMap.updateValue(newValue, forKey: "votesCount")
      }
    }

    public struct Thumbnail: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Media"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("url", type: .nonNull(.scalar(String.self))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(url: String) {
        self.init(unsafeResultMap: ["__typename": "Media", "url": url])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Public URL for the media object. Incase of videos this URL represents thumbnail generated from video.
      public var url: String {
        get {
          return resultMap["url"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "url")
        }
      }
    }
  }
}



