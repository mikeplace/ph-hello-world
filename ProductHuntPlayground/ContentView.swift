//
//  ContentView.swift
//  ProductHuntPlayground
//
//  Created by Michael Place on 9/6/21.
//

import SwiftUI
import Kingfisher

struct ContentView: View {
  @GraphQL(ProductHunt.posts)
  var posts: Paging<PostCell.Post>
  
  @ViewBuilder
  var body: some View {
    NavigationView {
      PagingView(posts, item: { post in
        PostCell(post: post)
      })
        .navigationTitle("Posts")
    }
  }
}

struct ThumbnailView: View {
  @GraphQL(ProductHunt.Post.thumbnail.url)
  var url
  
  var body: some View {
    if let url = url {
      KFImage(URL(string: url))
        .resizable()
        .frame(width: 60, height: 60)
        .cornerRadius(4)
    }
  }
}

struct PostCell: View {
  @GraphQL(ProductHunt.Post.name)
  var name
  
  @GraphQL(ProductHunt.Post.tagline)
  var tagline
  
  @GraphQL(ProductHunt.Post.votesCount)
  var votesCount
  
  @GraphQL(ProductHunt.Post._fragment)
  var post: ThumbnailView.Post
  
  var body: some View {
    HStack {
      HStack(spacing: 8) {
        ThumbnailView(post: post)
        VStack(alignment: .leading, spacing: 2) {
          Text(name).bold()
          Text(tagline).lineLimit(3)
        }
      }
      Spacer()
      Text("\(votesCount)")
        .padding(8)
        .overlay(
          RoundedRectangle(cornerRadius: 4)
            .stroke(Color.purple, lineWidth: 1)
        )
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
