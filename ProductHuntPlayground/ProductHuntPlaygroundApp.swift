//
//  ProductHuntPlaygroundApp.swift
//  ProductHuntPlayground
//
//  Created by Michael Place on 9/6/21.
//

import SwiftUI
import Apollo

struct MyRequestBodyCreator: RequestBodyCreator {
  public func requestBody<Operation: GraphQLOperation>(for operation: Operation, sendOperationIdentifiers: Bool,
    sendQueryDocument: Bool, autoPersistQuery: Bool) -> GraphQLMap {
    var body: GraphQLMap = [
      "variables": operation.variables?.filter { variable in variable.value != nil },
      "operationName": operation.operationName,
    ]

    if sendOperationIdentifiers {
      guard let operationIdentifier = operation.operationIdentifier else {
        preconditionFailure("To send operation identifiers, Apollo types must be generated with operationIdentifiers")
      }

      body["id"] = operationIdentifier
    }

    if sendQueryDocument {
      body["query"] = operation.queryDocument
    }

    if autoPersistQuery {
      guard let operationIdentifier = operation.operationIdentifier else {
        preconditionFailure("To enable `autoPersistQueries`, Apollo types must be generated with operationIdentifiers")
      }

      body["extensions"] = [
        "persistedQuery" : ["sha256Hash": operationIdentifier, "version": 1]
      ]
    }

    return body
  }
}

@main
struct ProductHuntPlaygroundApp: App {
  let api: ProductHunt = {
    let cache = InMemoryNormalizedCache()
    let store = ApolloStore(cache: cache)
    let client = URLSessionClient()
    let provider = LegacyInterceptorProvider(client: URLSessionClient(), store: store)
    let useGETForQueries = false
    let enableAutoPersistedQueries = false
    let useGETForPersistedQueryRetry = false
    
    let requestBodyCreator = MyRequestBodyCreator();
    
    let networkTransport = RequestChainNetworkTransport(
      interceptorProvider: provider,
      endpointURL: URL(string: "https://api.producthunt.com/v2/api/graphql")!,
      additionalHeaders: [
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer Pmi_WfEQ_Ag4tNWbQtcF_qMl3mOlB3YKXuMp545JmUg",
        "Host": "api.producthunt.com",
      ],
      autoPersistQueries: enableAutoPersistedQueries,
      requestBodyCreator: requestBodyCreator,
      useGETForQueries: useGETForQueries,
      useGETForPersistedQueryRetry: useGETForPersistedQueryRetry
    )
    
    let apolloClient = ApolloClient(networkTransport: networkTransport, store: ApolloStore())
    
    return ProductHunt(client: apolloClient)
  }()
  
  var body: some Scene {
    WindowGroup {
      api.contentView()
    }
  }
}
